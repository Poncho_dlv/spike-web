[![DeepSource](https://deepsource.io/bb/Poncho_dlv/spike-web.svg/?label=active+issues&show_trend=true&token=_1TDW-suuIckgspyVVIrX2uV)](https://deepsource.io/bb/Poncho_dlv/spike-web/?ref=repository-badge)

# SpikeWeb
# Quick Start

**Python 3.7 or higher is required**

You can clone Spike repo with your git client.

```ml
git clone https://bitbucket.org/Poncho_dlv/spike-web.git .
```

Install requirement

```ml
pip install -r requirements.txt
```
Add submodule
```ml
git submodule update --init
git submodule update --remote
```