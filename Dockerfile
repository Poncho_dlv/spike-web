FROM tiangolo/meinheld-gunicorn:latest

COPY ./app /app

# Set the working directory
WORKDIR /app

# Install the dependencies
RUN pip install --no-cache-dir -r requirements.txt