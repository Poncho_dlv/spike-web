platform_display = {"1": "BB2 Desktop", "2": "BB2 Playstation 4", "3": "BB2 Xbox 1"}


def get_competition_logo(competition_data: dict, league_data: dict):
    if competition_data.get("custom_logo"):
        logo = competition_data.get("custom_logo")
    elif competition_data.get("logo"):
        logo_id = competition_data.get("logo").split(":")[2]
        logo_id = logo_id.replace(">", "")
        logo = "https://cdn.discordapp.com/emojis/{}.png".format(logo_id)
    elif league_data.get("custom_logo"):
        logo = league_data.get("custom_logo")
    elif league_data.get("logo"):
        logo = "https://spike.ovh/static/spike_resources/img/logos/Logo_{}.png".format(league_data.get("logo"))
    else:
        logo = "https://cdn.discordapp.com/emojis/{}.png".format("466589965707640843")
    return logo