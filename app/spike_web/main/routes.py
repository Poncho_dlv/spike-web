from crawlerdetect import CrawlerDetect
from flask import render_template, Blueprint, request, send_from_directory

from spike_database.DiscordGuildInfo import DiscordGuildInfo
from spike_settings.SpikeSettings import SpikeSettings

main = Blueprint('main', __name__)


@main.route("/")
@main.route("/home")
def home():
    return render_template('home.html')


@main.route("/api_status")
def get_cyanide_api_status():
    if SpikeSettings.is_sync_enabled():
        return "<span class=\"badge badge-pill badge-success\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Cyanide api is " \
               "up\">Up</span> "
    return "<span class=\"badge badge-pill badge-danger\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Cyanide api is down\">Down</span>"


@main.route("/servers")
def servers():
    user_agent = request.headers.get("user-agent")
    if "https://discordapp.com" in user_agent:
        return render_template('discord_views/servers.html')

    crawler_detect = CrawlerDetect(user_agent=user_agent)
    if crawler_detect.isCrawler():
        return crawler_detect.getMatches()

    guild_db = DiscordGuildInfo()
    guilds = guild_db.get_all_guild()
    premium_guilds = []
    standard_guilds = []
    for guild in guilds:
        if not guild.get("premium") is True:
            standard_guilds.append(guild)
        else:
            premium_guilds.append(guild)
    premium_guilds = sorted(premium_guilds, key=lambda i: i['name'])
    standard_guilds = sorted(standard_guilds, key=lambda i: i["name"])

    return render_template('servers.html', title="Discord Servers", standard_guilds=standard_guilds, premium_guilds=premium_guilds)


@main.route("/link")
def link():
    return render_template('link.html', title="Links")


@main.route("/robots.txt")
@main.route("/sitemap.xml")
@main.route("/googlead38b810f6715414.html")
def static_from_root():
    return send_from_directory("static/", request.path[1:])


@main.route("/favicon.ico")
def get_fav_ico():
    return send_from_directory("static/spike_resources/img/misc/", "spike.ico")

