from operator import xor


def is_levelling_up(base_spp, spp_gain):
    triggerPoints = (6, 16, 31, 51, 76, 176)

    pre = base_spp
    post = pre + spp_gain

    # Which trigger points are passed pre and post match
    preMatch = (pre >= tp for tp in triggerPoints)
    postMatch = (post >= tp for tp in triggerPoints)

    # logical xor returns True only if there is a difference between pre and post match
    diff = map(xor, preMatch, postMatch)

    # If any elements of diff are True, have leveled up
    return any(diff)


def get_next_lvl(current_lvl):

    # Should only have a level between 1 and 7
    if current_lvl not in range(1, 8):
        return None

    return (6, 16, 31, 51, 76, 176, '*')[current_lvl - 1]