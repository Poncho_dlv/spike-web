from crawlerdetect import CrawlerDetect
from flask import render_template, Blueprint, request
from flask_mobility.decorators import mobile_template

from spike_database.Competitions import Competitions
from spike_database.Leagues import Leagues
from spike_requester.Utilities import Utilities as RequestUtils

matches = Blueprint('matches', __name__)


@matches.before_request
def before_request():
    user_agent = request.headers.get("user-agent")
    crawler_detect = CrawlerDetect(user_agent=user_agent)
    if "https://discordapp.com" not in user_agent and crawler_detect.isCrawler():
        return crawler_detect.getMatches()


@matches.route("/match", methods=['GET'])
@mobile_template('{mobile/}match.html')
def match(template):
    user_agent = request.headers.get("user-agent")

    match_id = request.args.get('match_uuid', None)
    if match_id is not None:
        match_model = RequestUtils.get_match(match_id)

        if match_model is not None:
            league_id = match_model.get_league_id()
            platform_id = match_model.get_platform_id()
            if league_id is not None and platform_id is not None:
                leagues_db = Leagues()
                logo = leagues_db.get_league_logo(league_id, platform_id)
                if logo is not None:
                    match_model.set_league_logo(logo)
                else:
                    match_model.set_league_logo("Neutre_04")
            else:
                match_model.set_league_logo("Neutre_04")

            competition_id = match_model.get_competition_id()
            if competition_id is not None and platform_id is not None:
                competition_db = Competitions()
                logo = competition_db.get_competition_logo(competition_id, platform_id)
                if logo is not None:
                    logo_id = logo.split(":")[2]
                    logo_id = logo_id.replace(">", "")
                    match_model.set_competition_logo(logo_id)
                else:
                    match_model.set_competition_logo("466589965707640843")
            else:
                match_model.set_competition_logo("466589965707640843")

            if "https://discordapp.com" in user_agent:
                return render_template('discord_views/match.html', match_model=match_model)
            return render_template(template, match_model=match_model)
        return render_template('error.html', error_code="Match not found", error_description="The requested match was not found on the server. If you entered the URL manually please check your spelling and try again.")
    return render_template('error.html', error_code="Match not found", error_description="The requested match was not found on the server. If you entered the URL manually please check your spelling and try again.")
