import logging
import os
from logging.handlers import RotatingFileHandler

# create log folder if not exist
if not os.path.exists("log"):
    os.makedirs("log")


spike_match_api_logger = logging.getLogger("spike_match_api")
spike_match_api_logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
file_handler = RotatingFileHandler('log/spike_match_api.log', 'a', 10000000, 1, encoding='utf-8')
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(formatter)
spike_match_api_logger.addHandler(file_handler)