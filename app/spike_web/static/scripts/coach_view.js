$('#switch_race_for_against').change(hide_show_race);

function load_overall(platform_id, coach_id, update_race=false) {
    //Fill overall view
    fetch('/platforms/' + platform_id + '/coaches/' + coach_id + '/overall_statistics')
    .then(response => response.text()).then(data => {
        let e = document.getElementById('overall_content');
        e.innerHTML=data;
        $('#overall_content').show();
        $('#overall_spinner').hide();
        if (update_race === true) {
            hide_show_race();
        }
    });
}

function load_ranked(platform_id, coach_id, update_race=false) {
    //Fill CCL view
    fetch('/platforms/' + platform_id + '/coaches/' + coach_id + '/ranked_statistics')
    .then(response => response.text()).then(data => {
        let e = document.getElementById('ranked_content');
        e.innerHTML=data;
        $('#ranked_content').show();
        $('#ranked_spinner').hide();
        if (update_race === true) {
            hide_show_race();
        }
    });
}

function load_league(platform_id, coach_id, update_race=false) {
    const element = document.getElementById("league_select")
    if (element != null && element.selectedIndex != null) {
        const current_league = element.options[element.selectedIndex].text;
        $('#league_content').hide();
        $('#league_spinner').show();
        $('#league_loading_text').text("Loading " + current_league + "...");
        //Fill competition view
        fetch('/platforms/' + platform_id + '/coaches/' + coach_id + '/1/' + current_league + '/statistics')
        .then(response => response.text()).then(data => {
            let e = document.getElementById('league_content');
            e.innerHTML=data;
            $('#league_content').show();
            $('#league_spinner').hide();
            if (update_race === true) {
                hide_show_race();
            }
        });
    }
}

function load_competition(platform_id, coach_id, update_race=false) {
    const element = document.getElementById("competition_select");
    if (element != null && element.selectedIndex != null) {
        const current_competition = element.options[element.selectedIndex].text;
        $('#competition_content').hide();
        $('#competition_spinner').show();
        $('#competition_loading_text').text("Loading " + current_competition + "...");
        //Fill competition view
        fetch('/platforms/' + platform_id + '/coaches/' + coach_id + '/2/' + current_competition + '/statistics')
        .then(response => response.text()).then(data => {
            let e = document.getElementById('competition_content');
            e.innerHTML=data;
            $('#competition_content').show();
            $('#competition_spinner').hide();
            if (update_race === true) {
                hide_show_race();
            }
        });
    }
}

function load_ranked_ladder(platform_id, coach_id, update_race=false) {
    const element = document.getElementById("ranked_ladder_select");
    if (element != null && element.selectedIndex != null) {
        const current_competition = element.options[element.selectedIndex].text;
        $('#ladder_content').hide();
        $('#ladder_spinner').show();
        $('#ladder_loading_text').text("Loading " + current_competition + "...");
        //Fill competition view
        fetch('/platforms/' + platform_id + '/coaches/' + coach_id + '/2/' + current_competition + '/statistics')
        .then(response => response.text()).then(data => {
            let e = document.getElementById('ladder_content');
            e.innerHTML=data;
            $('#ladder_content').show();
            $('#ladder_spinner').hide();
            if (update_race === true) {
                hide_show_race();
            }
        });
    }
}

function load_ranked_playoffs(platform_id, coach_id, update_race=false) {
    const element = document.getElementById("ccl_po_season_select");
    if (element != null && element.selectedIndex != null) {
        const current_competition = element.options[element.selectedIndex].text;
        $('#playoff_content').hide();
        $('#playoff_spinner').show();
        $('#playoff_loading_text').text("Loading " + current_competition + "...");
        //Fill competition view
        fetch('/platforms/' + platform_id + '/coaches/' + coach_id + '/2/' + current_competition + '/statistics')
        .then(response => response.text()).then(data => {
            let e = document.getElementById('playoff_content');
            e.innerHTML=data;
            $('#playoff_content').show();
            $('#playoff_spinner').hide();
            if (update_race === true) {
                hide_show_race();
            }
        });
    }
}

function hide_show_race() {
    const display_race_against = document.getElementById("switch_race_for_against").checked;
    if (display_race_against === true) {
        Array.from(document.getElementsByClassName("race_for")).forEach(
            function(element, index, array) {
                element.style.display = "none";
            }
        );
        Array.from(document.getElementsByClassName("race_against")).forEach(
            function(element, index, array) {
                element.style.display = "block";
            }
        );
    }
    else {
        Array.from(document.getElementsByClassName("race_for")).forEach(
            function(element, index, array) {
                element.style.display = "block";
            }
        );
        Array.from(document.getElementsByClassName("race_against")).forEach(
            function(element, index, array) {
                element.style.display = "none";
            }
        );
    }
}