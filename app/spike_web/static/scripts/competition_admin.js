function hide_show() {
    if($('#standing_source').val() === 'blood_bowl_2_standing'){
        $('#point_system_div').hide();
        $('#points_value').hide();
        $('#tie_breaker').hide();
        $('#admin_standing').hide();
    }else {
        $('#point_system_div').show();
        if ($('#point_system').val() === 'points') {
            $('#points_value').show();
            $('#tie_breaker').show();
            $('#admin_standing').show();
        }else {
            $('#points_value').hide();
            $('#tie_breaker').hide();
            $('#admin_standing').hide();
        }
    }
};

$('#standing_source').change(hide_show);
$('#point_system').change(hide_show);
$( document ).ready(hide_show);