function copy_clipboard(element_id, url) {
    navigator.clipboard.writeText('https://spike.ovh' + url).then(function() {
        document.getElementById(element_id).setAttribute('title', 'Copied');
    }, function() {
        document.getElementById(element_id).setAttribute('title', 'Unable to copy');
    });
}