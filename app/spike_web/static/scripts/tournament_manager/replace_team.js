function load_team(token) {

}

function replace_team(tournament_id, team_id, token) {
    const sub_button = document.getElementById("sub_button");
    sub_button.disabled = true;

    const new_team_id = document.getElementById("new_team_id").value

    fetch('/admin_tournaments/' + tournament_id + '/teams/' + team_id + '/save_replace', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': token
        },
        body: JSON.stringify({
            new_team_id: new_team_id,

        }),}).then(function(response) {
        if(response.ok) {
            response.json().then(function(data) {
                document.location.replace("/admin_tournaments/" + tournament_id + "/teams");
            });
        } else {
            sub_button.disabled = false;
        }
    }).catch(function(error) {
        sub_button.disabled = false;
    });
}