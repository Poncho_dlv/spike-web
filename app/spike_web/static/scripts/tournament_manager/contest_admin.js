function save_contest(tournament_id, round_index, contest_id, token) {
    const sub_button = document.getElementById("sub_button");
    sub_button.disabled = true;

    const score_home = document.getElementById("home_score").value
    const cas_home = document.getElementById("home_cas").value
    const kill_home = document.getElementById("home_kill").value

    const score_away = document.getElementById("away_score").value
    const cas_away = document.getElementById("away_cas").value
    const kill_away = document.getElementById("away_kill").value

    const lock_switch = document.getElementById("lock_switch").checked
    const naf_contest = document.getElementById("naf_contest").checked

    fetch('/admin_contests/' + contest_id + '/save_result', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': token
        },
        body: JSON.stringify({
            score_home: score_home,
            cas_home: cas_home,
            kill_home: kill_home,
            score_away: score_away,
            cas_away: cas_away,
            kill_away: kill_away,
            lock: lock_switch,
            naf: naf_contest
        }),}).then(function(response) {
        if(response.ok) {
            response.json().then(function(data) {
                document.location.replace("/admin_tournaments/" + tournament_id + "/rounds/" + round_index + "/contests");
            });
        } else {
            sub_button.disabled = false;
        }
    }).catch(function(error) {
        sub_button.disabled = false;
    });
}