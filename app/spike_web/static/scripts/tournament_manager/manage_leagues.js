function add_league(league_data) {
    document.getElementById("autocomplete").value = ""
    const close_id = 'delete_' + league_data.id
    let node = document.createElement("li");
    node.innerHTML='<li class="list-group-item d-flex league_entry" data-league_id=\"' + league_data.id + '\" data-league_name=\"' + league_data.name + '\"><p>' + league_data.name + '</p><button class=\"btn btn-outline-danger\ ml-auto" id=\"' + close_id + '\">&times;</button></li>';
    document.getElementById("league_container").appendChild(node);
    const close_btn = document.getElementById(close_id);

    close_btn.addEventListener("click", function() {
        this.parentElement.remove()
    });
}

function remove_league(element_id){
    let element = document.getElementById(element_id)
    element.remove()
}

$('#autocomplete').autocomplete({
    serviceUrl: '/search/leagues',
    onSelect: function (suggestion) {
        add_league(suggestion.data);
    }
});

function register_leagues(tournament_id, token){
    const sub_button = document.getElementById("sub_button");
    sub_button.disabled = true;
    let league_list = []

    Array.from(document.getElementsByClassName("league_entry")).forEach(
        function(element, index, array) {
            const data = {
                name: element.dataset.league_name,
                id: parseInt(element.dataset.league_id)
            }
            league_list.push(data)
        }
    );
    fetch('/admin_tournaments/' + tournament_id + '/update_leagues', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': token
        },
        body: JSON.stringify({leagues: league_list}),
    }).then(function(response) {
        if(response.ok) {
            response.json().then(function() {
                document.location.replace("/admin_tournaments?tournament_id=" + tournament_id);
            });
        } else {
            sub_button.disabled = false;
        }
    }).catch(function(error) {
        sub_button.disabled = false;
    });
}