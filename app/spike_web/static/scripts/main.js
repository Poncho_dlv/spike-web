$(document).ready(function(){
  //Fill cyanide api status
  fetch('/api_status').then(response => response.text()).then(data => {
    let e = document.getElementById('cyanide_api_status');
    e.innerHTML=data;
  });

});

//Get the button
var mybutton = document.getElementById("topButton");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  const path = window.location.pathname
  if (!path.includes("admin")) {
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
      mybutton.style.display = "block";
    } else {
      mybutton.style.display = "none";
    }
  }

}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}