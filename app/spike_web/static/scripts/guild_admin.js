document.getElementById("enable_automatic_role").onchange = function() {enable_disable_fields()};
document.getElementById("enable_welcome_leave_message").onchange = function() {enable_disable_fields()};
document.getElementById("enable_autoreg").onchange = function() {enable_disable_fields()};

function enable_disable_fields() {
  let state = document.getElementById("enable_automatic_role").checked;
  document.getElementById("new_user_role").disabled = !state;

  state = document.getElementById("enable_welcome_leave_message").checked;
  document.getElementById("welcome_message_channel").disabled = !state;
  document.getElementById("leave_message_channel").disabled = !state;
  document.getElementById("welcome_message").disabled = !state;
  document.getElementById("leave_message").disabled = !state;
  document.getElementById("leave_welcome_role_notification").disabled = !state;

  state = document.getElementById("enable_autoreg").checked;
  document.getElementById("autoreg_channel").disabled = !state;

}

$(document).ready(function(){
  enable_disable_fields();
});

function save_guild_settings(){
    
}