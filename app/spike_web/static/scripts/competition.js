$(document).ready(function(){
    compute_rank();
});

function compute_rank() {
    let win = Number(document.getElementById("win").value);
    if (isNaN(win)) { win = 0;}
    let draw = Number(document.getElementById("draw").value);
    if (isNaN(draw)) { draw = 0;}
    let loss = Number(document.getElementById("loss").value);
    if (isNaN(loss)) { loss = 0;}

    const games_played = win + draw + loss;
    const cross_point = 0.2;
    const limit = 42;
    const a = 0.05;
    const target = 28;
    const gamesBonus = 0.02;

    const expo = Math.log(a/(1-cross_point))/Math.log(1-target/limit);
    const win_pct = 100 * (win + draw/2)/games_played;
    const limFactor = cross_point+(1-cross_point)*(1-Math.pow (1-0.5*(games_played+limit-Math.sqrt(Math.pow(games_played-limit,2)))/limit, expo));
    const score = win_pct * limFactor + games_played*gamesBonus;
    let rank_points = Math.round(score * 100)/100;

    if (isNaN(rank_points)) {
        document.getElementById("rank").innerHTML = 0;
    }
    else {
        document.getElementById("rank").innerHTML = rank_points;
    }
}