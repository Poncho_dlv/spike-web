$('#autocomplete').autocomplete({
    serviceUrl: '/search/coaches',
    onSelect: function (suggestion) {
        document.location.replace("/link_coach?coach_id="+suggestion.data.id+"&platform_id="+suggestion.data.platform_id);
    }
});

function save_profile(token){
    const sub_button = document.getElementById("sub_button");
    sub_button.disabled = true;
    const country = document.getElementById("country").value;
    const youtube = document.getElementById("youtube").value;
    const naf_name = document.getElementById("naf_name").value;
    const naf_number = document.getElementById("naf_number").value;
    let lang_selected = [];
    for (let option of document.getElementById("lang").options) {
        if (option.selected) {
            lang_selected.push(option.value);
        }
    }

    fetch('/save_profile', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': token
        },
        body: JSON.stringify({
            country: country,
            lang: lang_selected,
            youtube: youtube,
            naf_name: naf_name,
            naf_number: naf_number
        }),}).then(function(response) {
        if(response.ok) {
            response.json().then(function(data) {
                document.location.replace("/home");
            });
        } else {
            sub_button.disabled = false;
            if(response.status === 460) {
                const msg_elem = document.getElementById("youtubeHelp");
                msg_elem.textContent = "Invalid Youtube channel";
                msg_elem.classList.add("text-danger");
                msg_elem.classList.remove("text-info");

                const youtube_elem = document.getElementById("youtube");
                youtube_elem.classList.add("border")
                youtube_elem.classList.add("border-danger")
            }

            if(response.status === 461) {
                const msg_elem = document.getElementById("steamHelp");
                msg_elem.textContent = "Invalid Steam profile";
                msg_elem.classList.add("text-danger");
                msg_elem.classList.remove("text-info");

                const youtube_elem = document.getElementById("steam");
                youtube_elem.classList.add("border")
                youtube_elem.classList.add("border-danger")
            }
        }
    }).catch(function(error) {
        sub_button.disabled = false;
    });
}