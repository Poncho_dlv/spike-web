$('#autocomplete').autocomplete({
    serviceUrl: '/search/coaches',
    onSelect: function (suggestion) {
        $('#coach_data').hide();
        $('#spinner').show();
        $('#loading_text').text("Loading " + suggestion.data.name + "...");
        document.location.replace("/coach?coach_id="+suggestion.data.id+"&platform_id="+suggestion.data.platform_id);
    }
});