function show_match_data(contest_id){
    const modal_content_elem = document.getElementById('modal_content');
    $('#modal_content').hide();
    $('#spinner').show();

    const modal = document.getElementById("myModal");
    modal.style.display = "block";
    fetch('/contests/' + contest_id)
        .then(response => response.text()).then(data => {
        modal_content_elem.innerHTML=data;
        $('#modal_content').show();
        $('#spinner').hide();
    });
}

function hide_match_data() {
    const modal = document.getElementById("myModal");
    modal.style.display = "none";

}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    const modal = document.getElementById("myModal");
    if (event.target === modal) {
        modal.style.display = "none";
    }
}
