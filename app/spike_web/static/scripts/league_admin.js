function add_admin(admin_data) {
    document.getElementById("autocomplete").value = ""
    const close_id = 'delete_' + admin_data.id
    let node = document.createElement("li");
    node.innerHTML='<li class="list-group-item d-flex admin_entry" data-admin_id=\"' + admin_data.id + '\" data-admin_name=\"' + admin_data.name + '\"><p>' + admin_data.name + '</p><button class=\"btn btn-outline-danger\ ml-auto" id=\"' + close_id + '\">&times;</button></li>';
    document.getElementById("admin_container").appendChild(node);
    const close_btn = document.getElementById(close_id);

    close_btn.addEventListener("click", function() {
        this.parentElement.remove()
    });
}

function remove_admin(element_id){
    let element = document.getElementById(element_id)
    element.remove()
}

$('#autocomplete').autocomplete({
    serviceUrl: '/search/users',
    onSelect: function (suggestion) {
        add_admin(suggestion.data);
    }
});

function save_league_parameters(league_id, platform_id, token){
    const sub_button = document.getElementById("sub_button");
    sub_button.disabled = true;
    let admin_list = []
    const discord_invite = document.getElementById("discord_invite").value
    const website_url = document.getElementById("website_url").value

    Array.from(document.getElementsByClassName("admin_entry")).forEach(
        function(element, index, array) {
            admin_list.push(element.dataset.admin_id)
        }
    );
    fetch('/save_league_settings', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': token
        },
        body: JSON.stringify({
            admins: admin_list,
            discord: discord_invite,
            website: website_url,
            league_id: league_id,
            platform_id: platform_id
        }),}).then(function(response) {
        if(response.ok) {
            response.json().then(function(data) {
                document.location.replace("/league?league="+league_id+"&platform="+platform_id);
            });
        } else {
            sub_button.disabled = false;
        }
    }).catch(function(error) {
        sub_button.disabled = false;
    });
}