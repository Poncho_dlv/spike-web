$('#tier_1_autocomplete').autocomplete({
    serviceUrl: '/vdg/search_competitions',
    onSelect: function (suggestion) {
        let container = document.getElementById("t1_container")
        add_competition(suggestion.data, container, 1);
        document.getElementById("tier_1_autocomplete").value = "";
    }
});

$('#tier_2_autocomplete').autocomplete({
    serviceUrl: '/vdg/search_competitions',
    onSelect: function (suggestion) {
        let container = document.getElementById("t2_container")
        add_competition(suggestion.data, container, 2);
        document.getElementById("tier_2_autocomplete").value = "";
    }
});

$('#tier_3_autocomplete').autocomplete({
    serviceUrl: '/vdg/search_competitions',
    onSelect: function (suggestion) {
        let container = document.getElementById("t3_container")
        add_competition(suggestion.data, container, 3);
        document.getElementById("tier_3_autocomplete").value = "";
    }
});

$('#tier_4_autocomplete').autocomplete({
    serviceUrl: '/vdg/search_competitions',
    onSelect: function (suggestion) {
        let container = document.getElementById("t4_container")
        add_competition(suggestion.data, container, 4);
        document.getElementById("tier_4_autocomplete").value = "";
    }
});


function add_competition(competition_data, container, tier_value){
    const close_id = 'delete_' + competition_data.id
    let node = document.createElement("li");
    node.innerHTML='<li class="list-group-item d-flex competition_entry" data-tier_value=\"' + tier_value + '\" data-competition_id=\"' + competition_data.id + '\" data-competition_name=\"' + competition_data.name + '\"><p>' + competition_data.name + '</p><button class=\"btn btn-outline-danger\ ml-auto" id=\"' + close_id + '\">&times;</button></li>';
    container.appendChild(node);
    const close_btn = document.getElementById(close_id);

    close_btn.addEventListener("click", function() {
        this.parentElement.remove()
    });
}


function save_tier_settings(token) {
    const sub_button = document.getElementById("sub_button");
    sub_button.disabled = true;
    let t1_list = []
    let t1_coefficient = document.getElementById("t1_coefficient").value
    if (t1_coefficient === ""){t1_coefficient=0}

    let t2_list = []
    let t2_coefficient = document.getElementById("t2_coefficient").value
    if (t2_coefficient === ""){t2_coefficient=0}

    let t3_list = []
    let t3_coefficient = document.getElementById("t3_coefficient").value
    if (t3_coefficient === ""){t3_coefficient=0}

    let t4_list = []
    let t4_coefficient = document.getElementById("t4_coefficient").value
    if (t4_coefficient === ""){t4_coefficient=0}

    Array.from(document.getElementsByClassName("competition_entry")).forEach(
        function(element, index, array) {
            if(Number(element.dataset.tier_value) === 1) {
                t1_list.push(JSON.stringify({id:element.dataset.competition_id, name: element.dataset.competition_name}))
            } else if (Number(element.dataset.tier_value) === 2) {
                t2_list.push(JSON.stringify({id:element.dataset.competition_id, name: element.dataset.competition_name}))
            } else if (Number(element.dataset.tier_value) === 3) {
                t3_list.push(JSON.stringify({id:element.dataset.competition_id, name: element.dataset.competition_name}))
            } else {
                t4_list.push(JSON.stringify({id:element.dataset.competition_id, name: element.dataset.competition_name}))
            }
        }
    );

    console.log(t1_list)

    fetch('/vdg/save_tier_settings', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': token
        },
        body: JSON.stringify({
            t1_list: t1_list,
            t1_coefficient: t1_coefficient,
            t2_list: t2_list,
            t2_coefficient: t2_coefficient,
            t3_list: t3_list,
            t3_coefficient: t3_coefficient,
            t4_list: t4_list,
            t4_coefficient: t4_coefficient,
        }),}).then(function(response) {
        if(response.ok) {
            response.json().then(function(data) {
                document.location.replace("/vdg");
            });
        } else {
            sub_button.disabled = false;
        }
    }).catch(function(error) {
        sub_button.disabled = false;
    });
}

function remove_competition(element_id){
    let element = document.getElementById(element_id)
    element.remove()
}