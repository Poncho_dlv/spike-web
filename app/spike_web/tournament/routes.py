from flask import render_template, Blueprint, request

from spike_database.BB2Resources import BB2Resources
from spike_requester.TournamentManagerAPI import TournamentManagerAPI

tournament = Blueprint('tournament', __name__)


@tournament.route("/tournaments/<tournament_id>")
def get_tournament(tournament_id: str):
    tournament_data = TournamentManagerAPI.get_tournament(tournament_id)
    standing = TournamentManagerAPI.get_standing(tournament_id)
    bb_rsrc = BB2Resources()

    user_agent = request.headers.get("user-agent")
    if "https://discordapp.com" in user_agent:
        return render_template("discord_views/tournament.html", tournament_data=tournament_data, tournament_id=tournament_id)

    if standing is None:
        standing = {
            "tournament_id": tournament_id,
            "standing": []
        }

    if tournament_data is not None:
        return render_template('tournaments/swiss.html', title=tournament_data.get("name"), tournament_id=tournament_id,
                               tournament_data=tournament_data, standing=standing, bb_rsrc=bb_rsrc)
    return render_template('error.html', error_code="Tournament not found",
                           error_description="The requested tournament was not found on the server. If you entered the URL manually please check your spelling and try again.")


@tournament.route("/tournaments/<tournament_id>/rounds/<round_index>")
def get_contests(tournament_id: str, round_index: int = 1):
    tournament_data = TournamentManagerAPI.get_tournament(tournament_id)
    contests = TournamentManagerAPI.get_contests(tournament_id, round_index, False, True)
    if contests is None:
        contests = []
    if tournament_data is not None:
        user_agent = request.headers.get("user-agent")
        if "https://discordapp.com" in user_agent:
            return render_template("discord_views/tournament.html", tournament_data=tournament_data, tournament_id=tournament_id)

        return render_template("tournaments/contests.html", title=tournament_data.get("name"), tournament_id=tournament_id,
                               tournament_data=tournament_data, contest_data=contests, round_index=round_index)
    return render_template('error.html', error_code="Tournament not found",
                           error_description="The requested tournament was not found on the server. If you entered the URL manually please check your spelling and try again.")


@tournament.route("/tournaments/<tournament_id>/teams")
def get_teams(tournament_id: str):
    tournament_data = TournamentManagerAPI.get_tournament(tournament_id)
    if tournament_data is not None:
        user_agent = request.headers.get("user-agent")
        if "https://discordapp.com" in user_agent:
            return render_template("discord_views/tournament.html", tournament_data=tournament_data, tournament_id=tournament_id)
        ret = TournamentManagerAPI.get_registered_teams(tournament_id, True)
        if ret is not None:
            ret["teams"] = sorted(ret.get("teams", []), key=lambda i: (i["status"], i["coach_name"].lower()))
            return render_template('tournaments/teams.html', title=tournament_data.get("name"), tournament_id=tournament_id,
                                   tournament_data=tournament_data, teams=ret.get("teams", []))
        return render_template('tournaments/teams.html', title=tournament_data.get("name"), tournament_id=tournament_id,
                               tournament_data=tournament_data, teams=[])
    return render_template('error.html', error_code="Tournament not found",
                           error_description="The requested tournament was not found on the server. If you entered the URL manually please check your spelling and try again.")
