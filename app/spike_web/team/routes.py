from crawlerdetect import CrawlerDetect
from flask import render_template, Blueprint, request
from flask_mobility.decorators import mobile_template

from spike_database.ResourcesRequest import ResourcesRequest
from spike_requester.CyanideApi import CyanideApi
from spike_requester.Utilities import Utilities
from spike_web.modules import Utilities as WebUtils

team = Blueprint('team', __name__)


@team.before_request
def before_request():
    user_agent = request.headers.get("user-agent")
    crawler_detect = CrawlerDetect(user_agent=user_agent)
    if "https://discordapp.com" not in user_agent and crawler_detect.isCrawler():
        return crawler_detect.getMatches()


@team.route("/team", methods=['GET'])
@mobile_template('{mobile/}team.html')
def get_team(template):
    user_agent = request.headers.get("user-agent")
    team_id = request.args.get("team_id")
    team_name = request.args.get("team_name")
    platform_arg = request.args.get("platform")
    match_uuid = request.args.get("match_uuid")
    display_team = request.args.get("display_team")

    if (match_uuid is not None and display_team in ["home", "away"]) or (platform_arg is not None and (team_id is not None or team_name is not None)):
        if "https://discordapp.com" in user_agent:
            cached_data = True
        else:
            cached_data = False
        rsrc_db = ResourcesRequest()
        if match_uuid is not None and display_team is not None:
            match = Utilities.get_match(match_uuid)
            platform_id = match.get_platform_id()
            platform = match.get_platform()
            if display_team == "home":
                team_id = match.get_team_home().get_id()
            else:
                team_id = match.get_team_away().get_id()
            team_model = Utilities.get_team(team_id=int(team_id), platform_id=platform_id, cached_data=cached_data)
        else:
            try:
                platform_id = int(platform_arg)
                platform = rsrc_db.get_platform_label(platform_id)
            except ValueError:
                platform_id = rsrc_db.get_platform_id(platform_arg)
                platform = platform_arg

            team_model = Utilities.get_team(team_name=team_name, team_id=int(team_id), platform_id=platform_id, cached_data=cached_data)

        if team_model is not None:
            match_list = []
            creation_date = team_model.get_creation_date().split(" ")[0]

            if "https://discordapp.com" in user_agent:
                return render_template('discord_views/team.html', team=team_model, platform=platform)

            last_matches = CyanideApi.get_team_matches(team_model.get_id(), start=creation_date, limit=5, platform=platform)
            if last_matches:
                for match in last_matches.get("matches", []):
                    match = Utilities.get_match(match.get("uuid"))
                    if match.get_coach_home().get_id() == 0:
                        match.data["coaches"][0]["coachname"] = "AI"
                    if match.get_coach_away().get_id() == 0:
                        match.data["coaches"][1]["coachname"] = "AI"
                    match.data["date"] = WebUtils.date_converter(match.get_start_datetime().split(" ")[0])
                    match_list.append(match)

                if len(match_list) > 0:
                    # Update mng with last match data
                    if not match_list[0].is_resurrection():
                        team_model.update_players_status(match_list[0])
            return render_template(template, title=team_model.get_name(), team=team_model, last_match=match_list, platform_id=platform_id)
        return render_template('error.html', error_code="Team not found",
                               error_description="The requested team was not found on the server, probably  deleted by his coach. "
                                                 "If you entered the URL manually please check your spelling and try again.")
    return render_template('error.html', error_code="Team not found",
                           error_description="The requested team was not found on the server. If you entered the URL manually please check your spelling and try again.")
