import logging

from crawlerdetect import CrawlerDetect
from flask import render_template, Blueprint, request, jsonify, redirect, url_for
from flask_login import current_user, login_required

from spike_database.Competitions import Competitions
from spike_database.Contests import Contests
from spike_database.Leagues import Leagues
from spike_database.ResourcesRequest import ResourcesRequest
from spike_requester.SpikeAPI.CclAPI import CclAPI
from spike_requester.Utilities import Utilities as RequestUtils
from spike_standings.SpikeEurOpen2 import EUROPEN2_LEAGUE_ID, EUROPEN2_BIS_LEAGUE_ID
from spike_standings.Utilities import Utilities
from spike_utilities.Utilities import Utilities as CommonUtils
from spike_web.competition.competition_form import AdminCompetition, TeamForm
from spike_web.modules.Utilities import date_converter
from web_utils.resources import platform_display, get_competition_logo

competition = Blueprint('competition', __name__)

spike_logger = logging.getLogger('spike_logger')


@competition.before_request
def before_request():
    user_agent = request.headers.get('user-agent')
    crawler_detect = CrawlerDetect(user_agent=user_agent)
    if 'https://discordapp.com' not in user_agent and crawler_detect.isCrawler():
        return crawler_detect.getMatches()


@competition.route('/search/competitions', methods=['GET'])
def search_competition():
    query = request.args.get('query')
    suggestions = {}
    if len(query) > 2:
        competition_db = Competitions()
        suggestions = competition_db.get_filtered_competitions_like(query, limit=9)
        for suggestion in suggestions:
            suggestion['value'] = '{} [{}]'.format(suggestion['value'], platform_display.get(str(suggestion['data']['platform_id'])))
        suggestions = jsonify({'suggestions': suggestions})
    return suggestions


@competition.route('/competition', methods=['GET'])
def get_competition():
    user_agent = request.headers.get('user-agent')

    competition_arg = request.args.get('competition', request.args.get('competition_id'))
    platform_arg = request.args.get('platform', request.args.get('platform_id'))
    match_uuid = request.args.get('match_uuid')

    if match_uuid is None and (platform_arg is None or competition_arg is None):
        return render_template('competitions/search.html')
    competition_db = Competitions()
    rsrc_db = ResourcesRequest()
    if match_uuid is not None:
        match = RequestUtils.get_match(match_uuid)
        platform_arg = match.get_platform_id()
        competition_arg = match.get_competition_id()
    try:
        platform_id = int(platform_arg)
    except ValueError:
        platform_id = rsrc_db.get_platform_id(platform_arg)

    try:
        competition_id = int(competition_arg)
        competition_data = competition_db.get_competition_data(competition_id, platform_id)
    except ValueError:
        competition_data = competition_db.get_competition_data_with_name(competition_arg, platform_id)

    if competition_data is not None:
        if competition_data.get('id') == 158165 and platform_id == 1:
            # Spike Europen custom page
            return redirect(url_for('sbbl.europen'))
        if competition_data.get('league_id') in [EUROPEN2_LEAGUE_ID, EUROPEN2_BIS_LEAGUE_ID] and platform_id == 1:
            return redirect(url_for('sbbl.europen2'))

        league_id = int(competition_data.get('league_id', 0))
        league_db = Leagues()
        league_data = league_db.get_league_data(league_id, platform_id)

        if league_data is not None:
            if current_user.is_authenticated and (current_user.id in league_data.get('admins', []) or current_user.is_admin()):
                display_admin = True
            else:
                display_admin = False

            competition_data['logo'] = get_competition_logo(competition_data, league_data)
            competition_data['label_format'] = competition_data.get('format', "").replace('_', ' ')
            competition_data['platform'] = rsrc_db.get_platform_label(platform_id)

            if 'https://discordapp.com' in user_agent:
                return render_template('discord_views/competition.html', competition_data=competition_data, league_data=league_data)

            if competition_data.get('format') in ['round_robin', 'swiss', 'ladder']:
                standing, point_system, standing_source = Utilities.get_standing(competition_data.get('id'), platform_id)
            else:
                standing = point_system = None

            if competition_data.get('format') in ['round_robin', 'swiss', 'single_elimination']:
                contests = CommonUtils.get_contest(competition_data.get('id'), platform_id, 'all')
                contests = sorted(contests, key=lambda i: (i['current_round'], i['contest_id']))
                split_contest = []
                try:
                    for contest in contests:
                        if contest.get('_id'):
                            contest['_id'] = str(contest['_id'])
                        index = int(contest['current_round'])
                        if len(split_contest) < index:
                            split_contest.append([])
                        split_contest[index - 1].append(contest)
                except IndexError:
                    split_contest = []

                if competition_data.get('round') is None or competition_data.get('rounds_count') is None and len(contests) > 0:
                    # Update info for old competition data with contest
                    round_count = contests[0].get('max_round', 0)
                    competition_data['rounds_count'] = round_count
                    competition_data['round'] = round_count
            else:
                split_contest = None

            is_cabalvision = CommonUtils.is_cabal_vision(league_id, platform_id)

            if competition_data.get('format') == 'single_elimination':
                if len(split_contest) > 0:
                    final = split_contest[-1][0]
                    if final.get('team_home', {}).get('score', 0) > final.get('team_away', {}).get('score', 0):
                        winner = final.get('team_home')
                    elif final.get('team_away', {}).get('score', 0) > final.get('team_home', {}).get('score', 0):
                        winner = final.get('team_away')
                    else:
                        winner = None
                else:
                    winner = None

                return render_template('competitions/tournament_bracket.html', title=competition_data.get('name'), league_data=league_data,
                                       competition_data=competition_data, contests=split_contest, display_admin=display_admin,
                                       is_cabalvision=is_cabalvision, CommonUtils=CommonUtils,
                                       winner=winner)
            if competition_data.get('format') == 'ladder':
                return render_template('competitions/ladder.html', title=competition_data.get('name'), league_data=league_data,
                                       competition_data=competition_data, standing=standing, display_admin=display_admin,
                                       is_cabalvision=is_cabalvision)
            if competition_data.get('format') in ['round_robin', 'swiss']:
                return render_template('competitions/robin_swiss.html', title=competition_data.get('name'), league_data=league_data,
                                       competition_data=competition_data, standing=standing,
                                       point_system=point_system, contests=split_contest, display_admin=display_admin)
            return render_template('competitions/default.html', title=competition_data.get('name'), league_data=league_data,
                                   competition_data=competition_data)
        return render_template('error.html', error_code='Competition not found',
                               error_description='The requested competition was not found on the server. If you entered the URL manually please '
                                                 'check your spelling and try again.')
    return render_template('error.html', error_code='Competition not found',
                           error_description='The requested competition was not found on the server. If you entered the URL manually please check '
                                             'your spelling and try again.')


@competition.route('/contests/<contest_id>', methods=['GET'])
def get_contest(contest_id: str):
    contest_db = Contests()
    contest_data = contest_db.get_contest_by_id(contest_id)
    if contest_data is not None:
        if contest_data.get('date'):
            date = contest_data.get('date').split(' ')[0]
            time = contest_data.get('date').split(' ')[1]
            contest_data['date'] = '{} - {} UTC'.format(date_converter(date), time)
        return render_template('competitions/contest_info.html', contest_data=contest_data)
    return ""


@competition.route('/competition_stat', methods=['GET'])
def competition_statistic():
    user_agent = request.headers.get('user-agent')

    competition_arg = request.args.get('competition', request.args.get('competition_id'))
    platform_arg = request.args.get('platform', request.args.get('platform_id'))

    if competition_arg is not None and platform_arg is not None:
        competition_db = Competitions()
        rsrc_db = ResourcesRequest()
        try:
            platform_id = int(platform_arg)
        except ValueError:
            platform_id = rsrc_db.get_platform_id(platform_arg)

        try:
            competition_id = int(competition_arg)
            competition_data = competition_db.get_competition_data(competition_id, platform_id)
        except ValueError:
            competition_data = competition_db.get_competition_data_with_name(competition_arg, platform_id)

        if competition_data is not None:
            league_id = int(competition_data.get('league_id', 0))
            if not CommonUtils.is_cabal_vision(league_id, platform_id):
                league_db = Leagues()
                league_data = league_db.get_league_data(league_id, platform_id)
                competition_data['platform'] = rsrc_db.get_platform_label(platform_id)

                competition_data['logo'] = get_competition_logo(competition_data, league_data)

                if 'https://discordapp.com' in user_agent:
                    return render_template('discord_views/competition.html', competition_data=competition_data, league_data=league_data)

                impact_players = Utilities.get_impact_player_ranking(competition_data.get('id'), platform_id)
                top_scorer = sorted(impact_players, key=lambda i: i['data']['Touchdowns'], reverse=True)
                top_blocker = sorted(impact_players, key=lambda i: i['data']['Blocks'], reverse=True)
                top_killer = sorted(impact_players, key=lambda i: i['data']['Kills'], reverse=True)
                most_violent = sorted(impact_players, key=lambda i: i['data']['Casualties'], reverse=True)
                top_surfer = sorted(impact_players, key=lambda i: i['data']['Surfs'], reverse=True)

                return render_template('competitions/competition_statistics.html', title=competition_data.get('name'), league_data=league_data,
                                       competition_data=competition_data,
                                       impact_players=impact_players[:10], top_scorer=top_scorer[:10], top_blocker=top_blocker[:10],
                                       top_killer=top_killer[:10], most_violent=most_violent[:10],
                                       top_surfer=top_surfer[:10])
            return render_template('error.html', error_code='Cabalvision Official League',
                                   error_description='Unable to display statistics for  Cabalvision Official League')
        return render_template('error.html', error_code='Competition not found',
                               error_description='The requested competition was not found on the server. If you entered the URL manually please '
                                                 'check your spelling and try again.')
    return render_template('error.html', error_code='Competition not found',
                           error_description='The requested competition was not found on the server. If you entered the URL manually please check '
                                             'your spelling and try again.')


@competition.route('/competition_top_races', methods=['GET'])
def competition_top_races():
    user_agent = request.headers.get('user-agent')

    competition_arg = request.args.get('competition', request.args.get('competition_id'))
    platform_arg = request.args.get('platform', request.args.get('platform_id'))

    if competition_arg is not None and platform_arg is not None:
        competition_db = Competitions()
        rsrc_db = ResourcesRequest()
        try:
            platform_id = int(platform_arg)
        except ValueError:
            platform_id = rsrc_db.get_platform_id(platform_arg)

        try:
            competition_id = int(competition_arg)
            competition_data = competition_db.get_competition_data(competition_id, platform_id)
        except ValueError:
            competition_data = competition_db.get_competition_data_with_name(competition_arg, platform_id)

        if competition_data is not None:
            league_id = int(competition_data.get('league_id', 0))
            if CommonUtils.is_cabal_vision(league_id, platform_id):
                league_db = Leagues()
                league_data = league_db.get_league_data(league_id, platform_id)
                competition_data['platform'] = rsrc_db.get_platform_label(platform_id)

                race_filter = request.args.get('race')
                limit = int(request.args.get('limit', 10))

                competition_data['logo'] = get_competition_logo(competition_data, league_data)

                if 'https://discordapp.com' in user_agent:
                    return render_template('discord_views/competition.html', competition_data=competition_data, league_data=league_data)

                formatted_top_race = {}

                for race in competition_data.get('top_races', []):
                    if race_filter is None or race == race_filter.replace(' ', ""):
                        race_name = "".join(map(lambda x: x if x.islower() else ' ' + x, race)).strip()
                        formatted_top_race[race_name] = competition_data['top_races'][race][:limit]

                return render_template('competitions/top_races.html', title=competition_data.get('name'), league_data=league_data,
                                       competition_data=competition_data, formatted_top_race=formatted_top_race)
            return render_template('error.html', error_code='Incompatible competition',
                                   error_description='Unable to display top races for this competition')
        return render_template('error.html', error_code='Competition not found',
                               error_description='The requested competition was not found on the server. If you entered the URL manually please '
                                                 'check your spelling and try again.')
    return render_template('error.html', error_code='Competition not found',
                           error_description='The requested competition was not found on the server. If you entered the URL manually please check '
                                             'your spelling and try again.')


@competition.route('/ranked_playoffs_prediction')
def champions_cup_prediction():
    user_agent = request.headers.get('user-agent')
    platform_id = int(request.args.get('platform_id', 1))
    rsrc_db = ResourcesRequest()
    competition_db = Competitions()

    season_data = CclAPI.get_current_season(platform_id=platform_id)
    if season_data.get('data') is None:
        return render_template('error.html', error_code='Competition not found', error_description=season_data.get('msg'))
    competition_id = season_data['data'].get('playoffs', {}).get('id')
    competition_data = competition_db.get_competition_data(competition_id, platform_id)

    if competition_data is not None:
        league_id = int(competition_data.get('league_id', 0))
        if CommonUtils.is_cabal_vision(league_id, platform_id):
            league_db = Leagues()
            league_data = league_db.get_league_data(league_id, platform_id)
            competition_data['platform'] = rsrc_db.get_platform_label(platform_id)

            competition_data['logo'] = get_competition_logo(competition_data, league_data)

            if 'https://discordapp.com' in user_agent:
                return render_template('discord_views/competition.html', competition_data=competition_data, league_data=league_data)

            predictions = CclAPI.get_predictions(platform_id)
            if predictions.get('data') is None:
                return render_template('error.html', error_code='Competition not found', error_description=predictions.get('msg'))

            race_breakdown = predictions['data'].get('race_breakdown', [])
            team_list = predictions['data'].get('team_list', [])

            return render_template('competitions/ranked_playoffs_prediction.html', title=competition_data.get('name'), league_data=league_data,
                                   competition_data=competition_data,
                                   team_list=team_list, race_breakdown=race_breakdown)
        return render_template('error.html', error_code='Incompatible competition',
                               error_description='Unable to display Champions cup estimation for this competition')
    return render_template('error.html', error_code='Competition not found',
                           error_description='The requested competition was not found on the server. If you entered the URL manually please check '
                                             'your spelling and try again.')


@competition.route('/admin_competition', methods=['GET', 'POST'])
@login_required
def admin_competition():
    form = AdminCompetition(request.form)
    if request.method == 'POST' and form.is_submitted():
        competition_db = Competitions()
        competition_id = int(form.competition_id.data)
        platform_id = int(form.platform_id.data)
        competition_data = competition_db.get_competition_data(competition_id, platform_id)

        if competition_data is not None:
            # save status
            status = int(form.status.data)
            competition_db.set_status(competition_id, platform_id, status)

            if competition_data.get('format') in ['round_robin', 'swiss']:
                standing_source = form.standing_source.data
                admin_data = {}
                if standing_source != 'blood_bowl_2_standing':
                    point_system = '{}-{}-{}'.format(form.win_point.data, form.draw_point.data, form.loss_point.data)
                    if form.tie_breaker1.data != "":
                        point_system += ',{}'.format(form.tie_breaker1.data)
                    if form.tie_breaker2.data != "":
                        point_system += ',{}'.format(form.tie_breaker2.data)
                    if form.tie_breaker3.data != "":
                        point_system += ',{}'.format(form.tie_breaker3.data)
                    if form.tie_breaker4.data != "":
                        point_system += ',{}'.format(form.tie_breaker4.data)
                    if form.tie_breaker5.data != "":
                        point_system += ',{}'.format(form.tie_breaker5.data)

                    teams_id = request.form.get('teams_id').split(',')
                    for team_id in teams_id:
                        admin_data[team_id] = {
                            'hide': True if request.form.get('hide_team_{}'.format(team_id), False) else False,
                            'point': int(request.form.get('point_{}'.format(team_id), 0)),
                            'win': int(request.form.get('win_{}'.format(team_id), 0)),
                            'draw': int(request.form.get('draw_{}'.format(team_id), 0)),
                            'loss': int(request.form.get('loss_{}'.format(team_id), 0)),
                            'td_for': int(request.form.get('td_for_{}'.format(team_id), 0)),
                            'td_against': int(request.form.get('td_against_{}'.format(team_id), 0)),
                            'casualties_inflicted': int(request.form.get('casualties_inflicted_{}'.format(team_id), 0)),
                            'casualties_sustained': int(request.form.get('casualties_sustained_{}'.format(team_id), 0)),
                            'external_tie_breaker': int(request.form.get('external_tie_breaker_{}'.format(team_id), 0))
                        }
                else:
                    point_system = ""
                competition_db.set_admin_standing(int(form.competition_id.data), int(form.platform_id.data), admin_data, standing_source,
                                                  point_system)
            return redirect(url_for('competition.get_competition', competition=form.competition_id.data, platform=form.platform_id.data))
        return render_template('error.html', error_code='Competition not found',
                               error_description='The requested competition was not found on the server. If you entered the URL manually please '
                                                 'check your spelling and try again.')

    competition_arg = request.args.get('competition', request.args.get('competition_id'))
    platform_arg = request.args.get('platform', request.args.get('platform_id'))

    if competition_arg is not None and platform_arg is not None:
        competition_db = Competitions()
        rsrc_db = ResourcesRequest()
        try:
            platform_id = int(platform_arg)
        except ValueError:
            platform_id = rsrc_db.get_platform_id(platform_arg)

        try:
            competition_id = int(competition_arg)
            competition_data = competition_db.get_competition_data(competition_id, platform_id)
        except ValueError:
            competition_data = competition_db.get_competition_data_with_name(competition_arg, platform_id)

        if competition_data is not None:
            league_id = competition_data.get('league_id', 0)
            league_db = Leagues()
            league_data = league_db.get_league_data(league_id, platform_id)

            if current_user.id not in league_data.get('admins', []) and not current_user.is_admin():
                return render_template('error.html', error_code='Forbidden', error_description='You are not allowed to admin this competition.')

            competition_data['logo'] = get_competition_logo(competition_data, league_data)

            team_list = []
            teams_id = []
            contests = CommonUtils.get_contest(competition_data.get('id'), platform_id, 'all')
            for contest in contests:
                home_team_id = contest.get('team_home', {}).get('team_id')
                if home_team_id and home_team_id not in teams_id:
                    teams_id.append(home_team_id)
                    team_list.append({
                        'team': contest.get('team_home', {}).get('team_name'),
                        'id': home_team_id,
                        'coach': contest.get('team_home', {}).get('coach_name'),
                        'logo': contest.get('team_home', {}).get('team_logo'),
                    })

                away_team_id = contest.get('team_away', {}).get('team_id')
                if away_team_id and away_team_id not in teams_id:
                    teams_id.append(away_team_id)
                    team_list.append({
                        'team': contest.get('team_away', {}).get('team_name'),
                        'id': away_team_id,
                        'coach': contest.get('team_away', {}).get('coach_name'),
                        'logo': contest.get('team_away', {}).get('team_logo'),
                    })

            # Init form fields
            form.competition_id.data = competition_data.get('id')
            form.platform_id.data = competition_data.get('platform_id')
            form.status.data = str(competition_data.get('status', 2))

            if competition_data.get('format') in ['round_robin', 'swiss']:
                form.standing_source.data = competition_data.get('standing_source')

                if competition_data.get('point_system'):
                    if competition_data.get('point_system') == 'rank':
                        form.point_system.data = 'rank'
                    else:
                        form.point_system.data = 'points'
                        point_system = competition_data.get('point_system').split(',')
                        try:
                            form.win_point.data = int(point_system[0].split('-')[0])
                            form.draw_point.data = int(point_system[0].split('-')[1])
                            form.loss_point.data = int(point_system[0].split('-')[2])
                        except:
                            form.win_point.data = 3
                            form.draw_point.data = 1
                            form.loss_point.data = 0

                        if len(point_system) > 1:
                            form.tie_breaker1.data = point_system[1]
                        if len(point_system) > 2:
                            form.tie_breaker2.data = point_system[2]
                        if len(point_system) > 3:
                            form.tie_breaker3.data = point_system[3]
                        if len(point_system) > 4:
                            form.tie_breaker4.data = point_system[4]
                        if len(point_system) > 5:
                            form.tie_breaker5.data = point_system[5]
                else:
                    form.point_system.data = 'rank'
                    form.win_point.data = 3
                    form.draw_point.data = 1
                    form.loss_point.data = 0

                teams_form = []
                form.teams_id.data = []
                for team in team_list:
                    team_form = TeamForm()
                    team_key = str(team.get('id'))
                    form.teams_id.data.append(team_key)
                    if team_key != "":
                        team_form.team_logo = team.get('logo')
                        team_form.team_name = team.get('team')
                        team_form.coach = team.get('coach')

                        team_form.team_id.id = 'team_id_{}'.format(team.get('id'))
                        team_form.team_id.name = team_form.team_id.id
                        team_form.team_id.data = team.get('id')

                        team_form.hide_team.id = 'hide_team_{}'.format(team.get('id'))
                        team_form.hide_team.name = team_form.hide_team.id
                        team_form.hide_team.data = competition_data.get('admin_standing', {}).get(team_key, {}).get('hide', False)

                        team_form.point.id = 'point_{}'.format(team.get('id'))
                        team_form.point.name = team_form.point.id
                        team_form.point.data = competition_data.get('admin_standing', {}).get(team_key, {}).get('point', 0)

                        team_form.win.id = 'win_{}'.format(team.get('id'))
                        team_form.win.name = team_form.win.id
                        team_form.win.data = competition_data.get('admin_standing', {}).get(team_key, {}).get('win', 0)

                        team_form.draw.id = 'draw_{}'.format(team.get('id'))
                        team_form.draw.name = team_form.draw.id
                        team_form.draw.data = competition_data.get('admin_standing', {}).get(team_key, {}).get('draw', 0)

                        team_form.loss.id = 'loss_{}'.format(team.get('id'))
                        team_form.loss.name = team_form.loss.id
                        team_form.loss.data = competition_data.get('admin_standing', {}).get(team_key, {}).get('loss', 0)

                        team_form.td_for.id = 'td_for_{}'.format(team.get('id'))
                        team_form.td_for.name = team_form.td_for.id
                        team_form.td_for.data = competition_data.get('admin_standing', {}).get(team_key, {}).get('td_for', 0)

                        team_form.td_against.id = 'td_against_{}'.format(team.get('id'))
                        team_form.td_against.name = team_form.td_against.id
                        team_form.td_against.data = competition_data.get('admin_standing', {}).get(team_key, {}).get('td_against', 0)

                        team_form.casualties_inflicted.id = 'casualties_inflicted_{}'.format(team.get('id'))
                        team_form.casualties_inflicted.name = team_form.casualties_inflicted.id
                        team_form.casualties_inflicted.data = competition_data.get('admin_standing', {}).get(team_key, {}).get('casualties_inflicted',
                                                                                                                               0)

                        team_form.casualties_sustained.id = 'casualties_sustained_{}'.format(team.get('id'))
                        team_form.casualties_sustained.name = team_form.casualties_sustained.id
                        team_form.casualties_sustained.data = competition_data.get('admin_standing', {}).get(team_key, {}).get('casualties_sustained',
                                                                                                                               0)

                        team_form.external_tie_breaker.id = 'external_tie_breaker_{}'.format(team.get('id'))
                        team_form.external_tie_breaker.name = team_form.external_tie_breaker.id
                        team_form.external_tie_breaker.data = competition_data.get('admin_standing', {}).get(team_key, {}).get('external_tie_breaker',
                                                                                                                               0)

                        teams_form.append(team_form)
                form.teams_id.data = ','.join(form.teams_id.data)
            else:
                teams_form = None

            return render_template('competitions/admin_competition.html', title=competition_data.get('name'), competition_data=competition_data,
                                   form=form, teams_form=teams_form)
        return render_template('error.html', error_code='Competition not found',
                               error_description='The requested competition was not found on the server. If you entered the URL manually please '
                                                 'check your spelling and try again.')
    return render_template('error.html', error_code='Competition not found',
                           error_description='The requested competition was not found on the server. If you entered the URL manually please check '
                                             'your spelling and try again.')


@competition.route('/ranked_concede')
@login_required
def ranked_concede():
    user_agent = request.headers.get('user-agent')

    competition_arg = request.args.get('competition', request.args.get('competition_id'))
    platform_arg = request.args.get('platform', request.args.get('platform_id'))

    if competition_arg is not None and platform_arg is not None:
        competition_db = Competitions()
        rsrc_db = ResourcesRequest()
        try:
            platform_id = int(platform_arg)
        except ValueError:
            platform_id = rsrc_db.get_platform_id(platform_arg)

        try:
            competition_id = int(competition_arg)
            competition_data = competition_db.get_competition_data(competition_id, platform_id)
        except ValueError:
            competition_data = competition_db.get_competition_data_with_name(competition_arg, platform_id)

        if competition_data is not None:
            league_id = int(competition_data.get('league_id', 0))
            if CommonUtils.is_cabal_vision(league_id, platform_id):
                league_db = Leagues()
                league_data = league_db.get_league_data(league_id, platform_id)

                if current_user.id not in league_data.get('admins', []) and not current_user.is_admin():
                    return render_template('error.html', error_code='Forbidden', error_description='You are not allowed to admin this competition.')

                competition_data['platform'] = rsrc_db.get_platform_label(platform_id)
                competition_data['logo'] = get_competition_logo(competition_data, league_data)

                if 'https://discordapp.com' in user_agent:
                    return render_template('discord_views/competition.html', competition_data=competition_data, league_data=league_data)

                return render_template('competitions/ranked_ban_list.html', title=competition_data.get('name'), league_data=league_data,
                                       competition_data=competition_data)
            return render_template('error.html', error_code='Incompatible competition',
                                   error_description='Unable to display top races for this competition')
        return render_template('error.html', error_code='Competition not found',
                               error_description='The requested competition was not found on the server. If you entered the URL manually please '
                                                 'check your spelling and try again.')
    return render_template('error.html', error_code='Competition not found',
                           error_description='The requested competition was not found on the server. If you entered the URL manually please check '
                                             'your spelling and try again.')


@competition.route('/rank_calculator')
def get_rank_calculator():
    return render_template('rank_calculator.html')


@competition.route('/ranked_ladder')
def get_ranked_ladder():
    platform_id = int(request.args.get('platform_id'))
    season_data = CclAPI.get_current_season(platform_id=platform_id)
    if season_data.get('data') is None:
        return render_template('error.html', error_code='Competition not found', error_description=season_data.get('msg'))
    competition_id = season_data['data'].get('ladder', {}).get('id')
    return redirect(url_for('competition.get_competition', competition_id=competition_id, platform_id=platform_id))


@competition.route('/ranked_playoffs')
def get_ranked_playoffs():
    platform_id = int(request.args.get('platform_id'))
    season_data = CclAPI.get_current_season(platform_id=platform_id)
    if season_data.get('data') is None:
        return render_template('error.html', error_code='Competition not found', error_description=season_data.get('msg'))
    competition_id = season_data['data'].get('playoffs', {}).get('id')
    return redirect(url_for('competition.get_competition', competition_id=competition_id, platform_id=platform_id))
