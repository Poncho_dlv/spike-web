from flask_wtf import FlaskForm
from wtforms import SubmitField, SelectField, IntegerField, BooleanField, HiddenField

from spike_standings import tie_breaker, standing_source, point_system


class AdminCompetition(FlaskForm):
    competition_id = HiddenField("competition_id")
    platform_id = HiddenField("platform_id")

    teams_id = HiddenField("teams_id")

    standing_source = SelectField(label="Standing source", choices=standing_source)

    point_system = SelectField(label="Sorting method", choices=point_system)

    win_point = IntegerField(label="Win points")
    draw_point = IntegerField(label="Draw points")
    loss_point = IntegerField(label="Loss points")

    tie_breaker1 = SelectField(label="Tie breaker 1", choices=tie_breaker)
    tie_breaker2 = SelectField(label="Tie breaker 2", choices=tie_breaker)
    tie_breaker3 = SelectField(label="Tie breaker 3", choices=tie_breaker)
    tie_breaker4 = SelectField(label="Tie breaker 4", choices=tie_breaker)
    tie_breaker5 = SelectField(label="Tie breaker 5", choices=tie_breaker)

    status = SelectField(label="Status", choices=[("0", "Created"), ("1", "Running"), ("2", "Finished"), ("3", "Hide")])

    submit = SubmitField(label="Save")

    error_occurred = False


class TeamForm(FlaskForm):
    team_logo = None
    team_name = None
    coach = None
    team_id = HiddenField("team_id")
    hide_team = BooleanField()
    point = IntegerField()
    win = IntegerField()
    draw = IntegerField()
    loss = IntegerField()
    td_for = IntegerField()
    td_against = IntegerField()
    casualties_inflicted = IntegerField()
    casualties_sustained = IntegerField()
    external_tie_breaker = IntegerField()

    error_occurred = False
