#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime

import feedparser


def date_converter(input_date):
    date = datetime.strptime(input_date, "%Y-%m-%d")
    return date.strftime("%d/%m/%Y")


def is_valid_youtube_channel(youtube_channel):
    channel_id = extract_id_from_url(youtube_channel)
    rss_url = "https://www.youtube.com/feeds/videos.xml?channel_id={}".format(channel_id)
    rss = feedparser.parse(rss_url)
    return rss.status == 200


def is_valid_steam_profile(steam_profile):
    return True


def extract_id_from_url(url):
    if "/" in url:
        split_str = url.split("/")
        # Remove empty and space element in list
        split_str = ' '.join(split_str).split()
        account_id = split_str[-1]
    else:
        account_id = url
    if len(account_id) == 0:
        account_id = None
    return account_id

