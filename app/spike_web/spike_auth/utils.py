from flask import session
from requests_oauthlib import OAuth2Session

from spike_settings.SpikeSettings import SpikeSettings


def twitch_token_updater(token):
    session['twitch_oauth2_token'] = token


def make_twitch_session(token=None, state=None, scope=None):
    return OAuth2Session(
        client_id=SpikeSettings().get_twitch_client_id(),
        token=token,
        state=state,
        scope=scope,
        redirect_uri=SpikeSettings.get_twitch_callback_url(),
        auto_refresh_kwargs={
            'client_id': SpikeSettings().get_twitch_client_id(),
            'client_secret': SpikeSettings().get_twitch_secret(),
        },
        auto_refresh_url="https://id.twitch.tv/oauth2/token",
        token_updater=twitch_token_updater)


def youtube_token_updater(token):
    session['youtube_oauth2_token'] = token


def make_youtube_session(token=None, state=None, scope=None):
    return OAuth2Session(
        client_id="1087506640131-1jklsak4jcnssegq922sso070f8boon7.apps.googleusercontent.com",
        token=token,
        state=state,
        scope=scope,
        redirect_uri="http://127.0.0.1:5000/youtube_callback",
        auto_refresh_kwargs={
            'client_id': SpikeSettings().get_twitch_client_id(),
            'client_secret': SpikeSettings().get_twitch_secret(),
        },
        auto_refresh_url="https://oauth2.googleapis.com/token",
        token_updater=youtube_token_updater)
