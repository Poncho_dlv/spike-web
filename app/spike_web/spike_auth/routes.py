import random
import re
import string
from urllib import parse

import requests
from flask import url_for, redirect, request, Blueprint, session, flash
from flask_login import login_required, current_user

from spike_settings.SpikeSettings import SpikeSettings
from spike_user_db.Users import Users
from spike_utilities.Utilities import Utilities
from spike_web.spike_auth.utils import make_twitch_session, make_youtube_session

spike_auth = Blueprint("spike_auth", __name__)


@spike_auth.route("/disconnect_twitch", methods=["GET"])
@login_required
def disconnect_twitch():
    user_db = Users()
    user_db.remove_twitch_link(current_user.id)
    flash("Twitch account unlinked", "success")
    return redirect(url_for("users.profile"))


@spike_auth.route("/connect_twitch", methods=["GET"])
@login_required
def connect_twitch():
    letters = string.ascii_letters
    result_str = "".join(random.choice(letters) for i in range(80))
    generated_hash = Utilities.hash_string(result_str)
    discord = make_twitch_session(scope=["viewing_activity_read"], state=generated_hash)
    authorization_url, state = discord.authorization_url("https://id.twitch.tv/oauth2/authorize")
    if state == generated_hash:
        session["next"] = request.args.get("next")
        return redirect(authorization_url)
    flash("Enable to link your Twitch account", "danger")
    return redirect(url_for("users.profile"))


@spike_auth.route("/twitch_callback")
@login_required
def twitch_callback():
    if request.values.get("error"):
        flash("Enable to link your Twitch account", "danger")
    else:
        user_db = Users()

        state = request.args.get("state")
        code = request.args.get("code")
        scope = request.args.get("scope")
        twitch = make_twitch_session(state=state, scope=scope)
        token = twitch.fetch_token("https://id.twitch.tv/oauth2/token", client_secret=SpikeSettings().get_twitch_secret(),
                                   authorization_response=request.url, code=code, include_client_id=SpikeSettings.get_twitch_client_id())
        session["twitch_oauth2_token"] = token

        headers = {
            "Authorization": "Bearer {}".format(token.get("access_token")),
            "Client-ID": SpikeSettings.get_twitch_client_id()
        }
        twitch_user = requests.get("https://api.twitch.tv/helix/users", headers=headers).json()
        if twitch_user is not None and twitch_user.get("data", [{}])[0].get("display_name"):
            flash("Twitch account linked", "success")
            user_db.update_user(current_user.id, twitch=twitch_user.get("data", [{}])[0].get("display_name"))
        else:
            flash("Enable to link your Twitch account", "danger")
    return redirect(url_for("users.profile"))


@spike_auth.route("/connect_steam")
def connect_steam():
    params = {
        "openid.ns": "http://specs.openid.net/auth/2.0",
        "openid.identity": "http://specs.openid.net/auth/2.0/identifier_select",
        "openid.claimed_id": "http://specs.openid.net/auth/2.0/identifier_select",
        "openid.mode": "checkid_setup",
        "openid.return_to": SpikeSettings.get_steam_callback_url(),
        "openid.realm": SpikeSettings.get_steam_base_url()
    }
    steam_openid_url = "https://steamcommunity.com/openid/login"
    param_string = parse.urlencode(params)
    auth_url = steam_openid_url + "?" + param_string
    return redirect(auth_url)


@spike_auth.route("/steam_callback", methods=["GET", "POST"])
def steam_callback():
    if request.values.get("error"):
        flash("Enable to link your Twitch account", "danger")
    else:
        user_db = Users()
        steam_id_re = re.compile('steamcommunity.com/openid/id/(.*?)$')
        match = steam_id_re.search(request.args.get("openid.identity"))
        if match is not None:
            params = {
                "key": SpikeSettings.get_steam_api_key(),
                "steamids": match.group(1)
            }
            steam_user = requests.get("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0001/", params=params).json()
            if steam_user is not None and steam_user.get("response", {}).get("players", {}).get("player", [None])[0]:
                steam_user = steam_user["response"]["players"]["player"][0]
                user_data = {
                    "id": steam_user.get("steamid"),
                    "name": steam_user.get("personaname"),
                    "profile_url": steam_user.get("profileurl"),
                    "avatar_url": steam_user.get("avatar")
                }
                flash("Steam account linked", "success")
                user_db.update_user(current_user.id, steam=user_data)
            else:
                flash("Enable to link your Steam account", "danger")
        else:
            flash("Enable to link your Steam account", "danger")
    return redirect(url_for("users.profile"))


@spike_auth.route("/disconnect_steam")
@login_required
def disconnect_steam():
    user_db = Users()
    user_db.remove_steam_link(current_user.id)
    flash("Steam account unlinked", "success")
    return redirect(url_for("users.profile"))


@spike_auth.route("/connect_youtube")
@login_required
def connect_youtube():
    return ""
    youtube = make_youtube_session(scope=["email", "openid", "profile"])
    authorization_url, state = youtube.authorization_url("https://accounts.google.com/o/oauth2/v2/auth")
    session["youtube_oauth2_state"] = state
    session['next'] = request.args.get('next')
    return redirect(authorization_url)


@spike_auth.route("/youtube_callback")
@login_required
def youtube_callback():
    return ""
    if request.values.get('error'):
        flash("Enable to link your Youtube account", "danger")

    youtube = make_youtube_session(state=session.get("youtube_oauth2_state"))
    token = youtube.fetch_token("https://oauth2.googleapis.com/token", client_secret="G-fidgKTKg3LGg7-x7feII8G", authorization_response=request.url)
    session["youtube_oauth2_token"] = token

    youtube = make_youtube_session(token=token)
    youtube_user = youtube.get("https://openidconnect.googleapis.com/v1/userinfo").json()
    # https://developers.google.com/identity/protocols/oauth2/scopes#youtube
