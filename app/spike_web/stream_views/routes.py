from flask import Blueprint, render_template

from spike_database.Contests import Contests
from spike_database.MatchHistory import MatchHistory
from spike_database.ResourcesRequest import ResourcesRequest
from spike_model.Contest import Contest
from spike_model.Team import Team
from spike_requester.CyanideApi import CyanideApi
from spike_utilities.Utilities import Utilities

stream = Blueprint('stream', __name__)


@stream.route("/stream/last_results/<platform_id>/<league_name>/<limit>", methods=['GET'])
def get_last_results(platform_id: int, league_name: str, limit: int = 10):
    match_history_db = MatchHistory()
    try:
        platform_id = int(platform_id)
    except:
        return ""

    last_match = match_history_db.get_last_match_in_league(league_name, platform_id, int(limit))
    return render_template("stream_views/last_results.html", last_match=last_match)


@stream.route("/stream/match_announcement/<contest_id>", methods=['GET'])
def get_match_announcement(contest_id: str):
    contest_db = Contests()
    contest = contest_db.get_contest_by_id(contest_id)
    rsrc = ResourcesRequest()
    if contest is not None:
        odds = {
            "home_odd": contest.get('odds', {}).get('home_win'),
            "draw_odd": contest.get('odds', {}).get('draw'),
            "away_odd": contest.get('odds', {}).get('away_win')
        }

        contest = Contest(contest)
        logo = Utilities.get_logo_id(contest.get_competition_logo())
        contest.set_competition_logo(logo)
        round_label = Utilities.get_round_label(contest.get_format(), contest.get_round(), contest.data.get("max_round"), "en")
        platform = rsrc.get_platform_label(contest.get_platform_id())
        home_team = CyanideApi.get_team(team_id=contest.data["team_home"]["team_id"], platform=platform)
        away_team = CyanideApi.get_team(team_id=contest.data["team_away"]["team_id"], platform=platform)
        home_sponsor = None
        away_sponsor = None
        stadium_upgrade = None

        if home_team and away_team:
            for card in home_team.get("team", {}).get("cards", []):
                if card["type"] == "Building":
                    stadium_upgrade = card["name"]
                elif card["type"] == "Sponsor":
                    home_sponsor = card["name"]

            for card in away_team.get("team", {}).get("cards", []):
                if card["type"] == "Sponsor":
                    away_sponsor = card["name"]

            home_team = Team(home_team)
            home_team.init_team_from_team_ws()
            away_team = Team(away_team)
            away_team.init_team_from_team_ws()

        return render_template("stream_views/match_announcement.html", contest=contest, round_label=round_label, home_team=home_team,
                               away_team=away_team, home_sponsor=home_sponsor, away_sponsor=away_sponsor, stadium_upgrade=stadium_upgrade, odds=odds)
    return ""
