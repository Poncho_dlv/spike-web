import json

from crawlerdetect import CrawlerDetect
from flask import render_template, Blueprint, request, jsonify, url_for, redirect, flash
from flask_login import current_user, login_required

from spike_database.Competitions import Competitions
from spike_database.Leagues import Leagues
from spike_standings.VDGStanding import VDGStanding, VDG_LEAGUE_ID, PLATFORM_ID

vdg = Blueprint('vdg', __name__)


@vdg.before_request
def before_request():
    user_agent = request.headers.get("user-agent")
    crawler_detect = CrawlerDetect(user_agent=user_agent)
    if "https://discordapp.com" not in user_agent and crawler_detect.isCrawler():
        return crawler_detect.getMatches()


@vdg.route("/vdg")
def home():
    return redirect(url_for('league.get_league', league_id=VDG_LEAGUE_ID, platform_id=PLATFORM_ID))


@vdg.route("/vdg/top_view")
def top_view():
    league_db = Leagues()
    competition_db = Competitions()
    league_data = league_db.get_league_data(VDG_LEAGUE_ID, PLATFORM_ID)

    if league_data is None:
        return ""

    competition_list = competition_db.get_competitions_in_league(VDG_LEAGUE_ID, PLATFORM_ID)
    active_competition = []

    for competition in competition_list:
        competition["label_format"] = competition.get("format", "").replace("_", " ")
        competition_logo = competition.get("logo")
        if competition_logo is not None:
            logo_id = competition_logo.split(":")[2]
            logo_id = logo_id.replace(">", "")
            competition["logo"] = "https://cdn.discordapp.com/emojis/{}.png".format(logo_id)
        else:
            competition["logo"] = "static/spike_resources/img/logos/Logo_{}.png".format(
                league_data.get("logo", "https://cdn.discordapp.com/emojis/{}.png".format("466589965707640843")))

        if competition.get("status") == 1:
            active_competition.append(competition)

    active_competition = sorted(active_competition, key=lambda i: str(i["name"]))
    return render_template('vdg/top_view.html', active_competition=active_competition)


@vdg.route("/vdg/main_view")
def main_view():
    user_agent = request.headers.get("user-agent")
    league_db = Leagues()
    league_data = league_db.get_league_data(VDG_LEAGUE_ID, PLATFORM_ID)
    if league_data.get("standing") is None:
        standing, tmp, tmp = VDGStanding.get_standing()
    else:
        standing = league_data.get("standing", [])
    if league_data is not None:
        if "https://discordapp.com" in user_agent:
            return render_template('discord_views/vdg.html', league_data=league_data)
        return render_template("vdg/standing.html", standing=standing, platform_id=PLATFORM_ID)
    return ""


@vdg.route("/vdg/search_competitions")
def search_competitions():
    query = request.args.get('query')
    suggestions = {}
    if len(query) > 2:
        competition_db = Competitions()
        suggestions = competition_db.get_filtered_competitions_like(query, 9, VDG_LEAGUE_ID, PLATFORM_ID)
        suggestions = jsonify({"suggestions": suggestions})
    return suggestions


@vdg.route("/vdg_admin", methods=['GET'])
@login_required
def administration():
    user_agent = request.headers.get("user-agent")
    league_db = Leagues()
    league_data = league_db.get_league_data(VDG_LEAGUE_ID, PLATFORM_ID)

    if league_data is not None:
        if current_user.id not in league_data.get("admins", []) and not current_user.is_admin():
            return render_template('error.html', error_code="Forbidden", error_description="You are not allowed to admin this league.")

        league_data["logo"] = url_for('static', filename='spike_resources/img/misc/vdg.png')

        if "https://discordapp.com" in user_agent:
            return render_template('discord_views/league.html', league_data=league_data)

        return render_template('vdg/admin.html', title=league_data.get("name", None), league_data=league_data)


@vdg.route("/vdg/save_tier_settings", methods=['POST'])
@login_required
def save_tier_settings():
    league_db = Leagues()
    content = request.json
    league_data = league_db.get_league_data(VDG_LEAGUE_ID, PLATFORM_ID)

    if league_data is not None:
        if current_user.id not in league_data.get("admins", []) and not current_user.is_admin():
            flash('You are not allowed to do that', 'danger')
            return {"error": "Forbidden"}, 403

        t1_list = []
        content["t1_coefficient"] = float(content.get("t1_coefficient", 0.))
        for competition in content.get("t1_list", []):
            competition = json.loads(competition)
            competition["id"] = int(competition["id"])
            t1_list.append(competition)
        content["t1_list"] = t1_list

        t2_list = []
        content["t2_coefficient"] = float(content.get("t2_coefficient", 0.))
        for competition in content.get("t2_list", []):
            competition = json.loads(competition)
            competition["id"] = int(competition["id"])
            t2_list.append(competition)
        content["t2_list"] = t2_list

        t3_list = []
        content["t3_coefficient"] = float(content.get("t3_coefficient", 0.))
        for competition in content.get("t3_list", []):
            competition = json.loads(competition)
            competition["id"] = int(competition["id"])
            t3_list.append(competition)
        content["t3_list"] = t3_list

        t4_list = []
        content["t4_coefficient"] = float(content.get("t4_coefficient", 0.))
        for competition in content.get("t4_list", []):
            competition = json.loads(competition)
            competition["id"] = int(competition["id"])
            t4_list.append(competition)
        content["t4_list"] = t4_list

        league_db.set_custom_data(VDG_LEAGUE_ID, PLATFORM_ID, content)
        flash('League parameters saved!', 'success')
        return {"success": True}, 200

    flash('Unknown league', 'danger')
    return {"error": "Bad Request"}, 400
