#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import logging
import os
from logging.handlers import RotatingFileHandler

import logging_loki
from flask import Flask
from flask_login import LoginManager
from flask_wtf.csrf import CSRFProtect

from flask_session import Session

csrf = CSRFProtect()
login_manager = LoginManager()
login_manager.login_view = "users.login"

spike_logger = logging.getLogger("spike_logger")
spike_logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt="%(asctime)s :: %(levelname)s :: %(message)s", datefmt="%Y-%m-%dT%H:%M:%S%z")
file_handler = RotatingFileHandler(filename="log/spike_web.log", maxBytes=10000000, backupCount=10, encoding="utf-8")
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)
spike_logger.addHandler(file_handler)
spike_logger.addHandler(stream_handler)

spike_requester_logger = logging.getLogger("spike_cyanide_requester")
spike_requester_logger.setLevel(logging.INFO)
loki_handler = logging_loki.LokiHandler(url="http://Loki:3100/loki/api/v1/push", tags={"application": "spike_web"}, version="1")
loki_handler.setLevel(logging.INFO)
spike_requester_logger.addHandler(loki_handler)


def create_app():
    with open('settings/spike_settings.json') as f:
        config = json.load(f)

    app = Flask(__name__)
    app.config.update(config)
    csrf.init_app(app)
    login_manager.init_app(app)
    Session(app)

    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = str(app.config.get("DEBUG", False))

    from spike_web.admin.routes import admin
    from spike_web.bounty.routes import bounty
    from spike_web.coaches.routes import coaches
    from spike_web.competition.routes import competition
    from spike_web.discord_guild.routes import discord_guild
    from spike_web.errors.handlers import errors
    from spike_web.league.routes import league
    from spike_web.main.routes import main
    from spike_web.matches.routes import matches
    from spike_web.naf.routes import naf
    from spike_web.scheduling.routes import scheduling
    from spike_web.sbbl.routes import sbbl
    from spike_web.tournament.routes import tournament
    from spike_web.vdg.routes import vdg
    from spike_web.team.routes import team
    # from spike_web.team_creator.routes import team_creator
    from spike_web.tournament_manager.routes import tm_admin
    from spike_web.users.routes import users
    from spike_web.spike_auth.routes import spike_auth

    app.register_blueprint(admin)
    app.register_blueprint(bounty)
    app.register_blueprint(coaches)
    app.register_blueprint(competition)
    app.register_blueprint(discord_guild)
    app.register_blueprint(errors)
    app.register_blueprint(league)
    app.register_blueprint(main)
    app.register_blueprint(matches)
    app.register_blueprint(naf)
    app.register_blueprint(scheduling)
    app.register_blueprint(sbbl)
    app.register_blueprint(vdg)
    app.register_blueprint(spike_auth)
    app.register_blueprint(team)
    app.register_blueprint(tournament)
    # app.register_blueprint(team_creator)
    app.register_blueprint(tm_admin)
    app.register_blueprint(users)

    # Stream overlays
    from spike_web.stream_views.routes import stream
    app.register_blueprint(stream)

    return app
