from flask import Blueprint, render_template
from flask_wtf.csrf import CSRFError

errors = Blueprint("errors", __name__)


@errors.app_errorhandler(404)
def error_404(error):
    return render_template("error.html", title="Error 404", error_code=404, error_description=error), 404


@errors.app_errorhandler(403)
def error_403(error):
    return render_template('error.html', title="Error 403", error_code=403, error_description=error), 403


@errors.app_errorhandler(500)
def error_500(error):
    return render_template('error.html', title="Error 500", error_code=500, error_description=error), 500


@errors.app_errorhandler(CSRFError)
def handle_csrf_error(e):
    return render_template('error.html', title="Error 400", error_code=400, error_description=e.description), 400
