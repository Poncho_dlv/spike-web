from crawlerdetect import CrawlerDetect
from flask import render_template, Blueprint, request, redirect, url_for
from flask_login import current_user, login_required

from spike_database.Competitions import Competitions
from spike_database.Contests import Contests
from spike_model.Contest import Contest
from spike_utilities.Utilities import Utilities as SpikeUtils
from spike_web.modules import Utilities

scheduling = Blueprint('scheduling', __name__)


@scheduling.before_request
def before_request():
    user_agent = request.headers.get("user-agent")
    crawler_detect = CrawlerDetect(user_agent=user_agent)
    if crawler_detect.isCrawler():
        return crawler_detect.getMatches()


@scheduling.route("/myscheds")
@login_required
def my_scheds():
    contest_db = Contests()
    competition_db = Competitions()
    contest_list = []
    if current_user.blood_bowl_coaches:
        coach_list = current_user.blood_bowl_coaches
        for current_coach in coach_list:
            contests = contest_db.get_contest_data_with_coach(current_coach["coach_id"], current_coach["platform_id"])
            for contest in contests:
                competition_data = competition_db.get_competition_data(contest["competition_id"], current_coach["platform_id"])
                if competition_data.get("logo"):
                    contest["competition_logo"] = competition_data.get("logo")
                if contest["current_round"] == competition_data.get("round"):
                    contest = Contest(contest)
                    logo = SpikeUtils.get_logo_id(contest.get_competition_logo())
                    contest.set_competition_logo(logo)

                    contest_date = contest.get_date()
                    if contest_date is not None:
                        date = contest_date.split(" ")[0]
                        time = contest_date.split(" ")[1]
                        contest.set_date("{} - {} UTC".format(Utilities.date_converter(date), time))
                    contest_list.append(contest)
    return render_template('myscheds.html', title="My Scheduled Games", contest_list=contest_list)


@scheduling.route("/sched_match", methods=['POST'])
@login_required
def scheduling_match():
    if request.method == "POST":
        schedule_date = request.form.get("schedule_date")
        schedule_time = request.form.get("schedule_time")
        new_date = "{} {}".format(schedule_date, schedule_time)
        contest_id = request.form.get("contest_id")
        platform_id = request.form.get("platform_id")
        contest_db = Contests()
        contest_db.update_contest_date(int(contest_id), int(platform_id), new_date)
    return redirect(url_for("scheduling.my_scheds"))
