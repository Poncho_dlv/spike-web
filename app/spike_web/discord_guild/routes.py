from flask import render_template, Blueprint, request, redirect, url_for, session
from flask_login import current_user, login_required

from spike_database.DiscordGuild import DiscordGuild
from spike_database.DiscordGuildInfo import DiscordGuildInfo
from spike_database.ResourcesRequest import ResourcesRequest
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_settings.SpikeSettings import SpikeSettings
from spike_utilities.Utilities import Utilities
from spike_web.discord_guild.forms import GuildConfig
from spike_web.users.utils import make_session

discord_guild = Blueprint('discord_guild', __name__)


@discord_guild.route("/myleagues", methods=['GET'])
@login_required
def my_leagues():
    user_guilds = session.get("guilds")
    if user_guilds is None:
        return render_template('error.html', error_code='Invalid state',
                               error_description='Enable to get current uer info, try to log out and login again.')

    guilds_id = []
    spike_guilds = Utilities.get_guilds_id_str()
    spike_guilds.remove('557471602799804436')
    for guild in user_guilds:
        if guild.get('id') in spike_guilds:
            guilds_id.append(guild.get('id'))

    guild_db = DiscordGuildInfo()
    guilds = guild_db.get_guilds(guilds_id)

    guilds = sorted(guilds, key=lambda i: i['name'])
    return render_template('discord_guilds/myguilds.html', title="My Leagues", guilds=guilds)


@discord_guild.route("/admin_guild", methods=['GET', 'POST'])
@login_required
def admin_discord():
    guild_info_db = DiscordGuildInfo()

    form = GuildConfig()
    if request.method == "POST" and form.is_submitted():
        guild_id = form.guild_id.data
        guild_data = guild_info_db.get_guild_data(guild_id)
        if guild_data is not None and (
                next((item for item in guild_data.get("admins", [{}]) if item is not None and item.get("id") == current_user.discord_id),
                     None) is not None or current_user.is_admin()):
            guild_settings = DiscordGuildSettings(guild_id)

            guild_settings.set_language(form.lang.data.lower())
            guild_settings.set_timezone(form.timezone.data)
            guild_settings.set_platform(form.league_platform.data.lower())

            guild_settings.set_bot_admin_role(int(form.admin_role.data)) if form.admin_role.data not in ["", "None", None] else None
            guild_settings.set_auto_role_enabled(form.enable_automatic_role.data)
            guild_settings.set_auto_role(int(form.new_user_role.data)) if form.new_user_role.data not in ["", "None",
                                                                                                          None] else guild_settings.set_auto_role(-1)
            guild_settings.set_welcome_enabled(form.enable_welcome_leave_message.data)
            guild_settings.set_welcome_message_channel(int(form.welcome_message_channel.data)) if form.welcome_message_channel.data not in ["",
                                                                                                                                            "None",
                                                                                                                                            None] else guild_settings.set_welcome_message_channel(
                -1)
            guild_settings.set_leave_message_channel(int(form.leave_message_channel.data)) if form.leave_message_channel.data not in ["", "None",
                                                                                                                                      None] else guild_settings.set_leave_message_channel(
                -1)
            guild_settings.set_welcome_notification_role(
                int(form.leave_welcome_role_notification.data)) if form.leave_welcome_role_notification.data not in ["", "None",
                                                                                                                     None] else guild_settings.set_welcome_notification_role(
                -1)
            guild_settings.set_welcome_message(form.welcome_message.data)
            guild_settings.set_leave_message(form.leave_message.data)

            guild_settings.set_compact_report(form.compact_report.data)
            guild_settings.set_display_impact_player(form.display_impact_player.data)
            guild_settings.set_enable_autoreg(form.enable_autoreg.data)
            guild_settings.set_autoreg_channel(int(form.autoreg_channel.data)) if form.autoreg_channel.data not in ["", "None",
                                                                                                                    None] else guild_settings.set_autoreg_channel(
                -1)

            guild_settings.set_bb_channel(int(form.blood_bowl_video_channel.data)) if form.blood_bowl_video_channel.data not in ["", "None",
                                                                                                                                 None] else guild_settings.set_bb_channel(
                -1)
            guild_settings.set_other_channel(int(form.other_video_channel.data)) if form.other_video_channel.data not in ["", "None",
                                                                                                                          None] else guild_settings.set_other_channel(
                -1)
            guild_settings.set_specific_channel(int(form.specific_channel.data)) if form.specific_channel.data not in ["", "None",
                                                                                                                       None] else guild_settings.set_specific_channel(
                -1)
            guild_settings.set_live_channel(int(form.live_channel.data)) if form.live_channel.data not in ["", "None",
                                                                                                           None] else guild_settings.set_live_channel(
                -1)
            guild_settings.set_specific_channel_keywords(form.video_keywords.data.strip().split(","))

            guild_settings.set_display_admin_bounty_only(form.display_admin_bounty.data)
            guild_settings.set_enable_bounty(form.enable_bounty.data)

        return redirect(url_for('discord_guild.my_leagues'))

    guild_id = request.args.get("guild_id")

    if guild_id is not None:
        guild_data = guild_info_db.get_guild_data(guild_id)
        guild_settings = DiscordGuildSettings(int(guild_id))

        if next((item for item in guild_data.get("admins", [{}]) if item.get("id") == current_user.discord_id),
                None) is not None or current_user.is_admin():

            roles = []
            for role in guild_data.get("roles", []):
                roles.append((str(role.get("id")), role.get("name")))
            roles.append(("", ""))
            roles.reverse()

            channels = [("", "")]
            for channel in guild_data.get("text_channels", []):
                channels.append((str(channel.get("id")), channel.get("name")))

            form.guild_id = guild_id
            form.lang.data = guild_settings.get_language().upper()
            form.timezone.data = guild_settings.get_timezone()
            form.league_platform.data = guild_settings.get_platform()
            form.enable_automatic_role.data = guild_settings.get_auto_role_enable()
            form.new_user_role.choices = roles
            form.new_user_role.data = str(guild_settings.get_auto_role())
            form.admin_role.choices = roles
            form.admin_role.data = str(guild_settings.get_bot_admin_role())

            form.welcome_message_channel.choices = channels
            form.welcome_message_channel.data = str(guild_settings.get_welcome_message_channel())
            form.leave_message_channel.choices = channels
            form.leave_message_channel.data = str(guild_settings.get_leave_message_channel())
            form.leave_welcome_role_notification.choices = roles
            form.leave_welcome_role_notification.data = str(guild_settings.get_welcome_notification_role())
            form.enable_welcome_leave_message.data = guild_settings.get_welcome_enabled()
            form.welcome_message.data = guild_settings.get_welcome_message()
            form.leave_message.data = guild_settings.get_leave_message()

            form.compact_report.data = guild_settings.get_compact_report()
            form.display_impact_player.data = guild_settings.get_display_impact_player()
            form.enable_autoreg.data = guild_settings.get_enable_autoreg()
            form.autoreg_channel.choices = channels
            form.autoreg_channel.data = str(guild_settings.get_autoreg_channel())

            form.blood_bowl_video_channel.choices = channels
            form.blood_bowl_video_channel.data = str(guild_settings.get_bb_channel())
            form.other_video_channel.choices = channels
            form.other_video_channel.data = str(guild_settings.get_other_channel())
            form.specific_channel.choices = channels
            form.specific_channel.data = str(guild_settings.get_specific_channel())
            keywords = guild_settings.get_specific_channel_keywords()
            if type(keywords) == list:
                keywords = ", ".join(keywords)
            form.video_keywords.data = keywords
            form.live_channel.choices = channels
            form.live_channel.data = str(guild_settings.get_live_channel())

            form.display_admin_bounty.data = guild_settings.get_display_admin_bounty_only()
            form.enable_bounty.data = guild_settings.get_enable_bounty()

            return render_template('discord_guilds/admin_guild.html', form=form, guild_data=guild_data)
        return render_template('error.html', error_code="Forbidden", error_description="You are not allowed to admin this discord server.")
    return render_template('error.html', error_code="Discord guild not found",
                           error_description="The requested guild was not found on the server. If you entered the URL manually please check your spelling and try again.")


@discord_guild.route("/get_guild_data/<guild_id>", methods=['GET'])
@login_required
def get_guild_data(guild_id):

    if session.get('members_data') is None:
        session['members_data'] = {}

    if session.get('members_data', {}).get(guild_id) is None:
        token = session.get("discord_oauth2_token")
        discord = make_session(token=token)
        member_data = discord.get(f"{SpikeSettings().get_discord_api_base_url()}/users/@me/guilds/{guild_id}/member").json()
        session['members_data'][guild_id] = member_data
    else:
        member_data = session['members_data'][guild_id]

    roles = member_data.get("roles", [])

    guild_settings = DiscordGuildSettings(guild_id)
    admin_role = str(guild_settings.get_bot_admin_role())
    display_admin = False
    if admin_role in roles:
        display_admin = True

    guilds = session.get("guilds")
    if guilds is None:
        return render_template('error.html', error_code='Invalid state', error_description='Enable to get user info, try to log out and login again.')

    guild_info_db = DiscordGuildInfo()
    guild_data = guild_info_db.get_guild_data(guild_id)

    guild_db = DiscordGuild(guild_id)
    rsrc_db = ResourcesRequest()

    league_list = guild_db.get_leagues_data()
    league_data = []
    for league in league_list:
        competitions_list = guild_db.get_competitions_data(league[1], league[3])
        league_json_data = {
            "name": league[2],
            "id": league[1],
            "logo": league[4],
            "platform_id": league[3],
            "platform": rsrc_db.get_platform_label(league[3]),
            "competitions": []
        }
        for competition in competitions_list:
            if "discordapp.com/api/emojis/" in competition[4]:
                logo = "https://cdn.discordapp.com/emojis/{}".format(str(competition[4]).split("/")[-1])
            else:
                logo = competition[4]
            competition_data = {
                "name": competition[2],
                "id": competition[1],
                "logo": logo
            }
            league_json_data["competitions"].append(competition_data)
        league_data.append(league_json_data)

    return render_template('discord_guilds/discord_guild.html', league_data=league_data, guild_data=guild_data, display_admin=display_admin)
