from flask_wtf import FlaskForm
from wtforms import SubmitField, HiddenField, SelectField, BooleanField, TextAreaField, StringField
from wtforms.validators import DataRequired
from spike_utilities.Utilities import Utilities


class GuildConfig(FlaskForm):
    guild_id = HiddenField("guild_id")

    lang = SelectField(label="Spike message lang", validators=[DataRequired()], choices=Utilities.get_lang())
    timezone = SelectField(label="Timezone", validators=[DataRequired()], choices=Utilities.get_timezones())
    league_platform = SelectField(label="League platform", validators=[DataRequired()], choices=[("pc", "Desktop"), ("ps4", "Playstation 4"), ("xb1", " Xbox One"), ("all", "All Platforms")])

    admin_role = SelectField(label="Administrator role", choices=[])
    enable_automatic_role = BooleanField(label="Enable automatic role when someone join your discord server")
    new_user_role = SelectField(label="Automatic role", choices=[])
    enable_welcome_leave_message = BooleanField(label="Enable welcome and leave notification")
    welcome_message_channel = SelectField(label="Welcome channel", choices=[])
    leave_message_channel = SelectField(label="Leave channel", choices=[])
    leave_welcome_role_notification = SelectField(label="Role notified", choices=[])
    welcome_message = TextAreaField(label="Welcome message")
    leave_message = TextAreaField(label="Leave message")

    compact_report = BooleanField(label="Compact match report")
    display_impact_player = BooleanField(label="Display impact players in match report")
    enable_autoreg = BooleanField(label="Enable competition automatic registration")
    autoreg_channel = SelectField(label="Match report channel for automatic registration", choices=[])

    blood_bowl_video_channel = SelectField(label="Blood bowl video channel", choices=[])
    other_video_channel = SelectField(label="Other game channel", choices=[])
    specific_channel = SelectField(label="League specific channel", choices=[])
    video_keywords = StringField(label="League keywords")
    live_channel = SelectField(label="Twitch live channel", choices=[])

    display_admin_bounty = BooleanField(label="Show bounty issued by administrator only")
    enable_bounty = BooleanField(label="Enable bounty")

    submit = SubmitField(label="Save")

    error_occurred = False
