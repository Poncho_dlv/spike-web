from flask import render_template, Blueprint
from flask_login import current_user, login_required

admin = Blueprint('admin', __name__)


@admin.route("/admin/logs", methods=['GET'])
@login_required
def get_logs():
    if current_user.is_admin():
        return ""
    return render_template('error.html', error_code="Forbidden", error_description="You are not allowed to to be there.")


@admin.route("/admin/users", methods=['GET'])
@login_required
def admin_users():
    if current_user.is_admin():
        return ""
    return render_template('error.html', error_code="Forbidden", error_description="You are not allowed to to be there.")
