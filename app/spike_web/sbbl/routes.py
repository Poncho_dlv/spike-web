from crawlerdetect import CrawlerDetect
from flask import render_template, Blueprint, request, url_for
from flask_login import current_user

from spike_database.Competitions import Competitions
from spike_database.CustomCompetitionTeam import CustomCompetitionTeam
from spike_database.Leagues import Leagues
from spike_database.ResourcesRequest import ResourcesRequest
from spike_standings.SpikeEurOpen2 import SpikeEurOpen2, EUROPEN2_COMPETITION_ID, EUROPEN2_LEAGUE_ID
from spike_standings.Utilities import Utilities
from spike_web.sbbl.PairingDb import PairingDb
from spike_user_db.Users import AccountLvl

sbbl = Blueprint('sbbl', __name__)

SPIKE_BLOOD_BOWL_LEAGUE_ID = 70929
SPIKE_BLOOD_BOWL_LEAGUE_ADMIN_ID = 71620
PLATFORM_ID = 1


@sbbl.before_request
def before_request():
    user_agent = request.headers.get("user-agent")
    crawler_detect = CrawlerDetect(user_agent=user_agent)
    if "https://discordapp.com" not in user_agent and crawler_detect.isCrawler():
        return crawler_detect.getMatches()


@sbbl.route("/europen")
@sbbl.route("/europen2")
def europen2():
    user_agent = request.headers.get("user-agent")
    league_db = Leagues()
    league_data = league_db.get_league_data(EUROPEN2_LEAGUE_ID, 1)
    competition_db = Competitions()
    competition_data = competition_db.get_competition_data(EUROPEN2_COMPETITION_ID, 1)

    if league_data is not None:
        if "https://discordapp.com" in user_agent:
            return render_template('discord_views/spike_europen.html', league_data=league_data)

        league_data["logo"] = url_for('static', filename='spike_resources/img/misc/sbbl.png')

        if current_user.is_authenticated and current_user.is_admin():
            display_admin = True
        else:
            display_admin = False

        standing, point_system, standing_source = Utilities.get_standing(EUROPEN2_COMPETITION_ID, 1)
        competition_data["standing"] = standing
        for team in standing:
            team["opponents_name"] = ""
            opponents = team.get("opponents", [])
            opponents.reverse()
            for rd_idx, opponent in enumerate(opponents, 1):
                match = next((item for item in standing if item.get("_id") == opponent), None)
                if match is not None:
                    team["opponents_name"] += "Round {}: {}&#013;".format(rd_idx, match.get("team_name"))

        return render_template('sbbl/europen2/home.html', title=league_data.get("name"), league_data=league_data, display_admin=display_admin, competition_data=competition_data)
    return render_template('error.html', error_code="League not found",
                           error_description="The requested league was not found on the server. If you entered the URL manually please check your spelling and try again.")


@sbbl.route("/europen2/teams")
def get_europen2_teams():
    user_agent = request.headers.get("user-agent")
    if "https://discordapp.com" in user_agent:
        return render_template('discord_views/spike_europen.html')
    custom_team_db = CustomCompetitionTeam()
    competition_db = Competitions()
    team_list = custom_team_db.get_teams(EUROPEN2_COMPETITION_ID, 1)
    competition_data = competition_db.get_competition_data(EUROPEN2_COMPETITION_ID, 1)
    league_db = Leagues()
    league_data = league_db.get_league_data(EUROPEN2_LEAGUE_ID, 1)
    competition_data["logo"] = url_for('static', filename='spike_resources/img/misc/sbbl.png')
    competition_data["platform"] = "pc"

    return render_template("sbbl/europen2/teams.html", title="The Spike Eur'Open 2", competition_data=competition_data, team_list=team_list, league_data=league_data)


@sbbl.route("/europen2/round/<round_index>")
def get_europen2_round(round_index: int = 1):
    user_agent = request.headers.get("user-agent")
    if "https://discordapp.com" in user_agent:
        return render_template('discord_views/spike_europen.html')

    competition_db = Competitions()
    competition_data = competition_db.get_competition_data(EUROPEN2_COMPETITION_ID, 1)
    league_db = Leagues()
    league_data = league_db.get_league_data(EUROPEN2_LEAGUE_ID, 1)
    competition_data["logo"] = "../../static/spike_resources/img/misc/sbbl.png"
    competition_data["platform"] = "pc"

    administrators = ["Obsidian", "Obsidian", "Obsidian", "Obsidian", "Obsidian", "Tyra", "Tyra", "Tyra", "Tyra", "Tyra", "Inkq", "Inkq", "Inkq", "Inkq", "Inkq", "Ungern", "Ungern", "Ungern",
                      "Ungern", "Ungern", "Ægir", "Ægir", "Ægir", "Ægir", "Ægir", "Alukhor", "Alukhor", "Alukhor", "Alukhor",
                      "Alukhor", "Poncho dlv", "Poncho dlv", "Poncho dlv", "Poncho dlv", "Poncho dlv"]

    pairing_db = PairingDb()
    contests = pairing_db.get_contest(EUROPEN2_COMPETITION_ID, int(round_index))
    return render_template("sbbl/europen2/pairing.html", title="The Spike Eur'Open 2", contests=contests, competition_data=competition_data, league_data=league_data, round_index=round_index,
                           administrators=administrators)


@sbbl.route("/europen1")
def europen():
    user_agent = request.headers.get("user-agent")
    if "https://discordapp.com" in user_agent:
        return render_template('discord_views/spike_europen.html')

    platform_id = 1
    competition_id = 158165
    rsrc_db = ResourcesRequest()
    competition_db = Competitions()
    competition_data = competition_db.get_competition_data(competition_id, platform_id)
    fake_competition_data = competition_db.get_competition_data(163358, 1)  # Wales

    if competition_data is not None and fake_competition_data is not None:
        league_db = Leagues()
        league_data = league_db.get_league_data(SPIKE_BLOOD_BOWL_LEAGUE_ID, platform_id)
        competition_data["logo"] = url_for('static', filename='spike_resources/img/misc/sbbl.png')
        competition_data["label_format"] = fake_competition_data.get("format", "").replace("_", " ")
        competition_data["platform"] = rsrc_db.get_platform_label(platform_id)
        competition_data["round"] = fake_competition_data.get("round")
        competition_data["rounds_count"] = fake_competition_data.get("rounds_count")
        competition_data["teams_count"] = fake_competition_data.get("teams_count")
        competition_data["teams_max"] = fake_competition_data.get("teams_max")

        standing, point_system, standing_source = Utilities.get_standing(competition_data.get("id"), platform_id)

        return render_template("sbbl/europen1/home.html", title=competition_data.get("name"), league_data=league_data,
                               competition_data=competition_data, standing=standing, point_system=point_system)
    return render_template('error.html', error_code="Competition not found",
                           error_description="The requested competition was not found on the server. If you entered the URL manually please check your spelling and try again.")
