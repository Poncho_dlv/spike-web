#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_database.BaseMongoDb import BaseMongoDb


class PairingDb(BaseMongoDb):
    def get_contest(self, tournament_id: int, round_index: int):
        self.open_database()
        collection = self.db["pairings"]
        query = {"tournament_id": tournament_id, "round_index": round_index}
        ret = collection.find_one(query, {"_id": 0, "contests": 1})
        self.close_database()
        if ret is not None:
            ret = ret.get("contests")
        return ret

    def is_public(self, tournament_id: int, round_index: int):
        self.open_database()
        collection = self.db["pairings"]
        query = {"tournament_id": tournament_id, "round_index": round_index}
        ret = collection.find_one(query, {"_id": 0, "public": 1})
        self.close_database()
        if ret is not None:
            ret = ret.get("public", False)
        else:
            ret = False
        return ret

