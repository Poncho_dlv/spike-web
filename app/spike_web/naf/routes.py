from crawlerdetect import CrawlerDetect
from flask import render_template, Blueprint, request, redirect, url_for

from spike_database.CustomCompetitionTeam import CustomCompetitionTeam
from spike_database.Leagues import Leagues

naf = Blueprint('naf', __name__)

NAF_LEAGUE = 89647
PLATFORM_ID = 1
WELSH_LEAGUE_ID = 92780


@naf.before_request
def before_request():
    user_agent = request.headers.get("user-agent")
    crawler_detect = CrawlerDetect(user_agent=user_agent)
    if "https://discordapp.com" not in user_agent and crawler_detect.isCrawler():
        return crawler_detect.getMatches()


@naf.route("/naf")
def home():
    return redirect(url_for('league.get_league', league_id=NAF_LEAGUE, platform_id=PLATFORM_ID))


@naf.route("/naf/main_view")
def main_view():
    return render_template("naf/main_view.html")


# Welsh Specific
@naf.route("/welsh")
def home_welsh():
    user_agent = request.headers.get("user-agent")
    league_db = Leagues()
    league_data = league_db.get_league_data(WELSH_LEAGUE_ID, PLATFORM_ID)

    if league_data is not None:
        if "https://discordapp.com" in user_agent:
            return render_template('discord_views/naf.html', league_data=league_data)

        league_data["logo"] = "static/spike_resources/img/misc/welsh_naf_banner.svg"

        return render_template('naf/welsh_championship_1/home.html', title=league_data.get("name"), league_data=league_data)
    return render_template('error.html', error_code="League not found",
                           error_description="The requested league was not found on the server. If you entered the URL manually please check your spelling and try again.")


@naf.route("/welsh/top_view")
def welsh_top_view():
    return render_template('naf/welsh_championship_1/top_view.html')


@naf.route("/welsh/main_view")
def welsh_main_view():
    return render_template('naf/welsh_championship_1/main_view.html')


@naf.route("/welsh/team_list")
def get_team_list():
    custom_team_db = CustomCompetitionTeam()
    team_list = custom_team_db.get_teams(205772, PLATFORM_ID)
    team_list = sorted(team_list, key=lambda i: str(i["members"][0]["coach_name"]))
    return render_template('naf/welsh_championship_1/team_list.html', team_list=team_list)


@naf.route("/welsh/round1")
def welsh_round1():
    return render_template("naf/welsh_championship_1/welsh_r1.html")


@naf.route("/welsh/round2")
def welsh_round2():
    return render_template("naf/welsh_championship_1/welsh_r2.html")


@naf.route("/welsh/round3")
def welsh_round3():
    return render_template("naf/welsh_championship_1/welsh_r3.html")


@naf.route("/welsh/round4")
def welsh_round4():
    return render_template("naf/welsh_championship_1/welsh_r4.html")


@naf.route("/welsh/round5")
def welsh_round5():
    return render_template("naf/welsh_championship_1/welsh_r5.html")


@naf.route("/welsh/round6")
def welsh_round6():
    return render_template("naf/welsh_championship_1/welsh_r6.html")
