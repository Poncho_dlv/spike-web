from flask import render_template, Blueprint, request, redirect, url_for
from flask_login import current_user, login_required

from spike_database.BB2Resources import BB2Resources
from spike_database.Bounty import Bounty
from spike_database.ResourcesRequest import ResourcesRequest
from spike_web.modules import Utilities

bounty = Blueprint('bounty', __name__)


@bounty.route("/mybounty", methods=['GET'])
@login_required
def my_bounty():
    rsrc_db = ResourcesRequest()
    bb_rsrc_db = BB2Resources()
    bounty_db = Bounty()
    bounty_list = bounty_db.get_all_bounty(current_user.discord_id)  # TODO replace by user_id
    for bnty in bounty_list:
        if not bnty.get("status"):
            bnty["status"] = 0
        if bnty.get("player_type"):
            bnty["player_type"] = bnty["player_type"].split("_")[1]
            bnty["player_type"] = rsrc_db.get_player_type_translation(bnty.get("player_type"), "en")

            bnty["race"] = bb_rsrc_db.get_race_label(bnty.get("race_id"))
        bnty["issue_date"] = Utilities.date_converter(bnty["issue_date"])
    return render_template('mybounty.html', title="My Bounty", bounty_list=bounty_list)


@bounty.route("/bounty", methods=['GET'])
@login_required
def custom_bounty():
    issuers_id = request.args.get('issuers')
    if issuers_id is not None:
        rsrc_db = ResourcesRequest()
        bb_rsrc_db = BB2Resources()
        issuers_id = issuers_id.split(",")
        issuers_id = list(map(int, issuers_id))

        bounty_db = Bounty()
        bounty_list = bounty_db.get_all_active_bounty(issuers_id)

        for bnty in bounty_list:
            if not bnty.get("status"):
                bnty["status"] = 0
            if bnty.get("player_type"):
                bnty["player_type"] = bnty["player_type"].split("_")[-1]
                bnty["player_type"] = rsrc_db.get_player_type_translation(bnty.get("player_type"), "en")

            bnty["race"] = bb_rsrc_db.get_race_label(bnty.get("race_id"))
            bnty["issue_date"] = Utilities.date_converter(bnty["issue_date"])

        return render_template('custom_bounty.html', title="Bounty", bounty_list=bounty_list)
    return render_template('error.html', error_code="Invalid request",
                           error_description="The requested bounty list was not found on the server. If you entered the URL manually please check your spelling and try again.")


@bounty.route("/remove_bounty", methods=['GET'])
@login_required
def remove_bounty():
    platform_id = request.args.get('platform_id')
    player_id = request.args.get('player_id')
    bounty_db = Bounty()
    bounty_db.remove_bounty_by_id(int(current_user.discord_id), int(player_id), int(platform_id))
    return redirect(url_for("bounty.my_bounty"))
