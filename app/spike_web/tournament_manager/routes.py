from datetime import datetime

from flask import render_template, Blueprint, request, redirect, url_for, flash, send_file
from flask_login import current_user, login_required

from spike_database.BB2Resources import BB2Resources
from spike_naf.XmlGenerator import NafXml
from spike_requester.TournamentManagerAPI import TournamentManagerAPI
from spike_requester.Utilities import Utilities
from spike_web.tournament_manager.forms import CreateTournament

tm_admin = Blueprint('tm_admin', __name__)


@tm_admin.before_request
@login_required
def before_request():
    if not current_user.is_tournament_director():
        return render_template('error.html', error_code="Forbidden", error_description="You are not allowed to to be there.")


@tm_admin.route("/admin_tournaments")
def tournaments():
    active_tournaments = TournamentManagerAPI.get_active_tournaments()
    return render_template('tournament_manager/home.html', active_tournaments=active_tournaments,
                           current_tournament=request.args.get("tournament_id"))


@tm_admin.route("/create_tournament", methods=['GET', 'POST'])
def create_tournament():
    form = CreateTournament(request.form)

    if request.method == "POST" and form.is_submitted():
        tie_breakers = []
        if form.tie_breaker_1.data != "":
            tie_breakers.append(form.tie_breaker_1.data)
        if form.tie_breaker_2.data != "":
            tie_breakers.append(form.tie_breaker_2.data)
        if form.tie_breaker_3.data != "":
            tie_breakers.append(form.tie_breaker_3.data)
        if form.tie_breaker_4.data != "":
            tie_breakers.append(form.tie_breaker_4.data)

        result = TournamentManagerAPI.create_tournament(tournament_name=form.tournament_name.data, tournament_format=form.format.data,
                                                        platform=form.platform.data, round_format=form.round_format.data,
                                                        number_of_round=form.number_of_round.data, win_value=form.win_value.data,
                                                        draw_value=form.draw_value.data, loss_value=form.loss_value.data, tie_breakers=tie_breakers)

        if result is not None and result.get("tournament_id") is not None:
            flash('Tournament created', 'success')
            return redirect(url_for('tm_admin.tournaments'))
    return render_template('tournament_manager/create_tournament.html', form=form)


@tm_admin.route("/admin_tournaments/<tournament_id>/delete", methods=['GET'])
def delete(tournament_id: str):
    ret = TournamentManagerAPI.delete_tournament(tournament_id)
    flash('Tournament deleted', 'danger')
    return redirect(url_for('tm_admin.tournaments'))


@tm_admin.route("/admin_tournaments/<tournament_id>/manage_leagues")
def manage_league(tournament_id: str):
    tournament_data = TournamentManagerAPI.get_tournament(tournament_id)
    tournament_data["id"] = tournament_id
    return render_template('tournament_manager/manage_leagues.html', tournament_data=tournament_data)


@tm_admin.route("/admin_tournaments/<tournament_id>/update_leagues", methods=['POST'])
def register_leagues(tournament_id: str):
    content = request.json
    ret = TournamentManagerAPI.update_registered_leagues(tournament_id, content.get("leagues", []))
    flash('League updated', 'success')
    return {"success": True}, 200


@tm_admin.route("/admin_tournaments/<tournament_id>/create_sign_up")
def create_sign_up(tournament_id: str):
    ret = TournamentManagerAPI.create_signup(tournament_id)
    if ret is not None:
        if ret.get("error") is None:
            flash('Sign up ladder created', 'success')
        else:
            flash(ret.get("error"), 'danger')
    else:
        flash("Unable to create Sign up ladder", 'danger')
    return redirect(url_for('tm_admin.tournaments', tournament_id=tournament_id))


@tm_admin.route("/admin_tournaments/<tournament_id>/register_teams")
def register_teams(tournament_id: str):
    ret = TournamentManagerAPI.register_teams(tournament_id)
    return redirect(url_for('tm_admin.manage_teams', tournament_id=tournament_id))


@tm_admin.route("/admin_tournaments/<tournament_id>/teams/<team_id>/unregister")
def unregister_team(tournament_id: str, team_id: str):
    ret = TournamentManagerAPI.unregister_team(team_id)
    if ret is not None and ret.get("success"):
        flash('Team unregistered', 'success')
    else:
        flash('Un-registration failed', 'danger')
    return redirect(url_for('tm_admin.manage_teams', tournament_id=tournament_id))


@tm_admin.route("/admin_tournaments/<tournament_id>/manage_teams")
def manage_teams(tournament_id: str):
    ret = TournamentManagerAPI.get_registered_teams(tournament_id)
    if ret is not None:
        ret["teams"] = sorted(ret.get("teams", []), key=lambda i: (i["coach_name"], i["status"]))
        return render_template('tournament_manager/teams.html', teams=ret.get("teams", []), tournament_id=tournament_id, admin=True)
    flash('No team registered', 'danger')
    return redirect(url_for('tm_admin.tournaments', tournament_id=tournament_id))


@tm_admin.route("/admin_tournaments/<tournament_id>/rounds/<round_index>/draw")
def draw_round(tournament_id: str, round_index: int):
    tournament_data = TournamentManagerAPI.get_tournament(tournament_id)
    if tournament_data is not None and tournament_data.get("current_round") == (int(round_index) - 1):
        ret = TournamentManagerAPI.get_contests(tournament_id, round_index, allow_draw=True)
        if ret is None:
            flash('Unable to draw', 'danger')
            return redirect(url_for('tm_admin.tournaments', tournament_id=tournament_id))

        team_list = TournamentManagerAPI.get_registered_teams(tournament_id, False)
        if team_list is None:
            flash('Unable to draw', 'danger')
            return redirect(url_for('tm_admin.tournaments', tournament_id=tournament_id))
        return render_template("tournament_manager/contests.html", tournament_id=tournament_id, contest_data=ret, round_index=round_index,
                               published=False)
    flash('Unable to draw', 'danger')
    return redirect(url_for('tm_admin.tournaments', tournament_id=tournament_id))


@tm_admin.route("/admin_tournaments/<tournament_id>/rounds/<round_index>/contests")
def contests(tournament_id: str, round_index: int):
    ret = TournamentManagerAPI.get_contests(tournament_id, round_index)
    if ret.get("error"):
        flash(ret.get("error"), 'danger')
        return redirect(url_for('tm_admin.tournaments', tournament_id=tournament_id))

    team_list = TournamentManagerAPI.get_registered_teams(tournament_id, False)
    if team_list is None:
        return render_template("tournament_manager/contests.html", tournament_id=tournament_id)

    published = True
    for contest in ret.get("contests", []):
        if not contest.get("public"):
            published = False
    return render_template("tournament_manager/contests.html", tournament_id=tournament_id, contest_data=ret, round_index=round_index,
                           published=published)


@tm_admin.route("/admin_tournaments/<tournament_id>/rounds/<round_index>/contests/<contest_id>/update_contest")
def edit_contest(tournament_id: str, round_index: int, contest_id: str):
    contest = TournamentManagerAPI.get_contest(contest_id)
    team_list = TournamentManagerAPI.get_registered_teams(tournament_id, False)
    if team_list is None:
        return render_template("tournament_manager/contests.html", tournament_id=tournament_id)
    team_list = team_list.get("teams", [])

    if contest is not None:
        home_team_data = next((team for team in team_list if team.get("_id") == contest.get("home_team", {}).get("id")), None)
        away_team_data = next((team for team in team_list if team.get("_id") == contest.get("away_team", {}).get("id")), None)
        contest["home_team_data"] = home_team_data
        contest["away_team_data"] = away_team_data

        return render_template("tournament_manager/contest_edit.html", contest=contest, tournament_id=tournament_id, round_index=round_index)


@tm_admin.route("/admin_contests/<contest_id>/save_result", methods=['POST'])
def save_contest_result(contest_id: str):
    content = request.json
    content["score_home"] = int(content["score_home"])
    content["score_away"] = int(content["score_away"])
    content["cas_home"] = int(content["cas_home"])
    content["cas_away"] = int(content["cas_away"])
    content["kill_home"] = int(content["kill_home"])
    content["kill_away"] = int(content["kill_away"])
    content["date"] = datetime.utcnow().strftime("%Y-%m-%d %H:%M")
    ret = TournamentManagerAPI.update_contest(contest_id, content)
    if ret is not None:
        return {"success": True}, 200
    return {"error": "Bad Request"}, 400


@tm_admin.route("/admin_tournaments/<tournament_id>/rounds/<round_index>/publish_round")
def publish_round(tournament_id: str, round_index: int):
    ret = TournamentManagerAPI.publish_round(tournament_id, round_index)
    return redirect(url_for('tm_admin.contests', tournament_id=tournament_id, round_index=round_index))


@tm_admin.route("/admin_tournaments/<tournament_id>/rounds/<round_index>/create_competitions")
def create_competitions(tournament_id: str, round_index: int):
    ret = TournamentManagerAPI.create_competitions(tournament_id, round_index)
    return redirect(url_for('tm_admin.contests', tournament_id=tournament_id, round_index=round_index))


@tm_admin.route("/admin_tournaments/<tournament_id>/standing")
def get_standing(tournament_id: str):
    standing = TournamentManagerAPI.get_standing(tournament_id)
    bb_rsrc = BB2Resources()
    if standing is None:
        standing = {
            "tournament_id": tournament_id,
            "standing": []
        }
    return render_template('tournament_manager/standing.html', standing=standing, bb_rsrc=bb_rsrc)


@tm_admin.route("/admin_tournaments/<tournament_id>/teams")
def teams(tournament_id: str):
    ret = TournamentManagerAPI.get_registered_teams(tournament_id, False)
    if ret is not None:
        ret["teams"] = sorted(ret.get("teams", []), key=lambda i: (i["status"], i["coach_name"]))
        return render_template('tournament_manager/teams.html', teams=ret.get("teams", []), tournament_id=tournament_id)
    return render_template('tournament_manager/teams.html', teams=[], tournament_id=tournament_id)


@tm_admin.route("/admin_tournaments/<tournament_id>/start")
def start_tournament(tournament_id: str):
    ret = TournamentManagerAPI.start_tournament(tournament_id)
    if ret is not None:
        if ret.get("error") is None:
            flash('Tournament started', 'success')
        else:
            flash(ret.get("error"), 'danger')
    else:
        flash("Unable to start tournament", 'danger')
    return redirect(url_for('tm_admin.tournaments', tournament_id=tournament_id))


@tm_admin.route("/admin_tournaments/<tournament_id>/close")
def close_tournament(tournament_id: str):
    ret = TournamentManagerAPI.close_tournament(tournament_id)
    if ret is not None:
        if ret.get("error") is None:
            flash('Tournament closed', 'success')
        else:
            flash(ret.get("error"), 'danger')
    else:
        flash("Unable to close tournament", 'danger')
    return redirect(url_for('tm_admin.tournaments'))


@tm_admin.route("/admin_tournaments/<tournament_id>/generate_xml")
def generate_xml(tournament_id: str):
    tournament_data = TournamentManagerAPI.get_tournament(tournament_id)
    if tournament_data is None:
        flash("Unknown tournament", 'danger')
        return redirect(url_for('tm_admin.tournaments'))

    contests = []
    for round_index in range(0, tournament_data.get("number_of_round", 0)):
        data = TournamentManagerAPI.get_contests(tournament_id, round_index + 1, allow_draw=False, public_only=True)
        contests.extend(data.get("contests"))

    xml_data = NafXml.generate_from_tournament(contests, "Kfoged")
    xml_file = open("naf.xml", "wt")
    xml_file.write(xml_data)
    xml_file.close()
    return send_file("../naf.xml", as_attachment=True)


@tm_admin.route("/admin_tournaments/<tournament_id>/teams/<team_id>/replace_team")
def replace_team(tournament_id: str, team_id: str):
    team_data = TournamentManagerAPI.get_team(team_id)
    if team_data is not None:
        return render_template('tournament_manager/replace_team.html', original_team=team_data, tournament_id=tournament_id)
    return redirect(url_for('tm_admin.manage_teams', tournament_id=tournament_id))


@tm_admin.route("/admin_tournaments/<tournament_id>/teams/<team_id>/save_replace", methods=['POST'])
def save_replace(tournament_id: str, team_id: str):
    content = request.json
    new_team_id = int(content["new_team_id"])
    team_model = Utilities.get_team(team_id=new_team_id, platform_id=1, cached_data=True)
    if team_model is None:
        team_model = Utilities.get_team(team_id=new_team_id, platform_id=1)

    if team_model is None:
        return {"error": "Invalid team"}, 500

    reg_team = TournamentManagerAPI.register_team(tournament_id, team_model.get_name(), team_model.get_id(), 1, team_model.get_logo(),
                                                  team_model.get_race_id(), team_model.get_coach().get_name(), team_model.get_coach().get_id())

    contest_list = TournamentManagerAPI.get_current_round(tournament_id)
    contest_id = None
    for contest in contest_list.get("contests", []):
        if team_id in [contest["home_team"]["id"], contest["away_team"]["id"]]:
            contest_id = contest["_id"]

    if contest_id is not None:
        ret = TournamentManagerAPI.replace_team(contest_id, team_id, reg_team["team"])
        if ret is not None:
            ret = TournamentManagerAPI.unregister_team(team_id)
            if ret is not None:
                return {"success": True}, 200
            return {"error": "Unable to remove team."}, 500
        return {"error": "No contest found."}, 500
    return {"success": True}, 200
