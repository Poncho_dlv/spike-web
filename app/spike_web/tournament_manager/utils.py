def build_dict_data(data_list: list):
    ret = {}
    for elem in data_list:
        ret[elem[0]] = elem[1]
    return ret
