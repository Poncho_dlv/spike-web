from flask_wtf import FlaskForm
from wtforms import SubmitField, IntegerField, StringField, SelectField
from wtforms.validators import DataRequired

tournament_format = [("single_player", "Single player"), ("team_tournament", "Team tournament")]
round_format = [("round_robin", "Round robin"), ("round_swiss", "Swiss")]
platform = [("bb2_pc", "Blood Bowl 2 PC")]
tie_breaker = [
    ("", ""),
    ("touchdown_scored", "Touchdowns scored"),
    ("touchdown_conceded", "Touchdowns conceded"),
    ("touchdown_difference", "Touchdown difference"),
    ("casualties_inflicted", "Casualties inflicted"),
    ("casualties_sustained", "Casualties sustained"),
    ("casualties_difference", "Casualties difference"),
    ("kill_inflicted", "Kill inflicted"),
    ("kill_sustained", "Kill sustained"),
    ("coleman_waldorf", "Coleman Waldorf"),
    ("coleman_waldorf_2", "Coleman Waldorf 2"),
    ("coleman_waldorf_3", "Coleman Waldorf 3"),
    ("coleman_waldorf_4", "Coleman Waldorf 4"),
    ("win", "Number of win"),
    ("draw", "Number of draw"),
    ("loss", "Number of loss"),
    ("head_to_head", "Head to Head")
]


class CreateTournament(FlaskForm):
    tournament_name = StringField(label="Tournament name", validators=[DataRequired()])
    format = SelectField(label="Format", validators=[DataRequired()], choices=tournament_format)
    platform = SelectField(label="Platform", validators=[DataRequired()], choices=platform)
    round_format = SelectField(label="Round format", validators=[DataRequired()], choices=round_format)
    number_of_round = IntegerField(label="Number of round", validators=[DataRequired()])
    win_value = IntegerField(label="Win points", validators=[DataRequired()])
    draw_value = IntegerField(label="Draw points", validators=[DataRequired()])
    loss_value = IntegerField(label="Loss points", validators=[DataRequired()])
    tie_breaker_1 = SelectField(label="First tie breaker", choices=tie_breaker)
    tie_breaker_2 = SelectField(label="Second tie breaker", choices=tie_breaker)
    tie_breaker_3 = SelectField(label="Third tie breaker", choices=tie_breaker)
    tie_breaker_4 = SelectField(label="Fourth tie breaker", choices=tie_breaker)

    submit = SubmitField(label="Create")
