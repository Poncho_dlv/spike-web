from bson.objectid import ObjectId
from crawlerdetect import CrawlerDetect
from flask import render_template, Blueprint, request, jsonify, redirect, url_for, flash, current_app
from flask_login import current_user, login_required

from spike_database.Competitions import Competitions
from spike_database.Leagues import Leagues
from spike_database.MatchHistory import MatchHistory
from spike_database.ResourcesRequest import ResourcesRequest
from spike_requester.SpikeAPI.CclAPI import CclAPI
from spike_requester.Utilities import Utilities
from spike_standings.SpikeEurOpen2 import EUROPEN2_LEAGUE_ID, EUROPEN2_BIS_LEAGUE_ID
from spike_standings.VDGStanding import VDG_LEAGUE_ID
from spike_user_db.Users import Users
from spike_utilities.Utilities import Utilities as CommonUtils
from spike_web.modules.Utilities import date_converter
from web_utils.resources import platform_display, get_competition_logo

league = Blueprint("league", __name__)


@league.before_request
def before_request():
    user_agent = request.headers.get("user-agent")
    crawler_detect = CrawlerDetect(user_agent=user_agent)
    if "https://discordapp.com" not in user_agent and crawler_detect.isCrawler():
        return crawler_detect.getMatches()


@league.route("/search/leagues", methods=["GET"])
def search_league():
    query = request.args.get("query")
    suggestions = {}
    if len(query) > 2:
        league_db = Leagues()
        suggestions = league_db.get_filtered_leagues_like(query, limit=9)
        for suggestion in suggestions:
            suggestion["value"] = "{} [{}]".format(suggestion["value"],
                                                   platform_display.get(str(suggestion["data"]["platform_id"])))
        suggestions = jsonify({"suggestions": suggestions})
    return suggestions


@league.route("/platforms/<platform_id>/leagues/<league_id>/top_view", methods=["GET"])
def get_top_view(platform_id: int, league_id: int):
    try:
        platform_id = int(platform_id)
        league_id = int(league_id)
    except:
        return ""

    if platform_id == 1 and league_id in [92780, 92781, 92840]:
        return redirect(url_for("naf.welsh_top_view", _external=True, _scheme=current_app.config["PREFERRED_URL_SCHEME"]))
    if platform_id == 1 and league_id == VDG_LEAGUE_ID:
        return redirect(url_for("vdg.top_view", _external=True, _scheme=current_app.config["PREFERRED_URL_SCHEME"]))

    match_history_db = MatchHistory()
    last_match = match_history_db.get_last_match_in_league_by_id(league_id, platform_id, 5)
    if len(last_match) == 0:
        return ''
    for match in last_match:
        match["date"] = date_converter(match["date"])
    return render_template("leagues/last_match.html", last_match=last_match)


@league.route("/platforms/<platform_id>/leagues/<league_id>/main_view", methods=["GET"])
def get_main_view(platform_id, league_id):
    try:
        league_id = int(league_id)
        platform_id = int(platform_id)
    except:
        return ""

    if platform_id == 1 and league_id == VDG_LEAGUE_ID:
        return redirect(url_for("vdg.main_view", _external=True, _scheme=current_app.config["PREFERRED_URL_SCHEME"]))
    if platform_id == 1 and league_id in [92780, 92781, 92840]:
        return redirect(url_for("naf.welsh_main_view", _external=True, _scheme=current_app.config["PREFERRED_URL_SCHEME"]))
    if platform_id == 1 and league_id == 89647:
        return redirect(url_for("naf.main_view", _external=True, _scheme=current_app.config["PREFERRED_URL_SCHEME"]))
    league_db = Leagues()
    competition_db = Competitions()
    league_data = league_db.get_league_data(league_id, platform_id)

    if league_data is None:
        return ""

    competition_list = competition_db.get_competitions_in_league(league_id, platform_id)
    active_competition = []
    inactive_competition = []

    for competition in competition_list:
        competition["label_format"] = competition.get("format", "").replace("_", " ")
        competition["logo"] = get_competition_logo(competition, league_data)

        if competition.get("status") == 1:
            active_competition.append(competition)
        elif competition.get("status") != 3:
            inactive_competition.append(competition)

    active_competition = sorted(active_competition, key=lambda i: str(i["name"]))
    inactive_competition = sorted(inactive_competition, key=lambda i: str(i["name"]))
    return render_template("leagues/competition_list.html", active_competition=active_competition, inactive_competition=inactive_competition)


@league.route("/league", methods=["GET"])
def get_league():
    user_agent = request.headers.get("user-agent")
    league_arg = request.args.get("league", request.args.get("league_id"))
    platform_arg = request.args.get("platform", request.args.get("platform_id"))
    match_uuid = request.args.get("match_uuid")
    former_champion = None
    display_winners_history = False

    if match_uuid is None and (platform_arg is None or league_arg is None):
        return render_template("leagues/search.html")
    if match_uuid is not None:
        match = Utilities.get_match(match_uuid)
        platform_arg = match.get_platform_id()
        league_arg = match.get_league_id()

    league_db = Leagues()
    rsrc_db = ResourcesRequest()
    try:
        platform_id = int(platform_arg)
    except ValueError:
        platform_id = rsrc_db.get_platform_id(platform_arg)

    try:
        league_id = int(league_arg)
        league_data = league_db.get_league_data(league_id, platform_id)
    except ValueError:
        league_data = league_db.get_league_data_with_name(league_arg, platform_id)

    if league_data is not None:
        league_id = league_data["id"]
        if platform_id == 1 and league_id in [EUROPEN2_LEAGUE_ID, EUROPEN2_BIS_LEAGUE_ID]:
            return redirect(url_for("sbbl.europen2"))
        if platform_id == 1 and league_id in [92780, 92781, 92840]:
            return redirect(url_for("naf.home_welsh"))

        league_data["platform"] = rsrc_db.get_platform_label(platform_id)
        if league_data.get("logo") is None and league_data.get("custom_logo") is None:
            league_data["logo"] = "Neutre_04"

        if "https://discordapp.com" in user_agent:
            return render_template("discord_views/league.html", league_data=league_data)

        if current_user.is_authenticated and (current_user.id in league_data.get("admins", []) or current_user.is_admin()):
            display_admin = True
        else:
            display_admin = False

        if CommonUtils.is_cabal_vision(league_id, platform_id):
            display_winners_history = True
            season_data = CclAPI.get_seasons(platform_id=platform_id)
            competition_db = Competitions()
            playoffs_list = season_data.get('data', {}).get("playoffs", [])
            playoffs_list.reverse()
            for playoff in playoffs_list:
                if playoff is not None:
                    competition_data = competition_db.get_competition_data(playoff.get("id"), platform_id)
                    if competition_data is not None:
                        former_champion = competition_data.get("winner")
                        if former_champion is not None:
                            break
        else:
            former_champion = league_data.get("former_champion")

        return render_template("leagues/league.html", title=league_data.get("name"), league_data=league_data, display_admin=display_admin,
                               former_champion=former_champion, display_winners_history=display_winners_history)
    return render_template("error.html", error_code="League not found",
                           error_description="The requested league was not found on the server. If you entered the URL manually please check "
                                             "your spelling and try again.")


@league.route("/save_league_settings", methods=["POST"])
@login_required
def save_league_settings():
    league_db = Leagues()
    content = request.json
    league_admins = []

    try:
        league_id = int(content.get("league_id"))
        platform_id = int(content.get("platform_id"))
        league_data = league_db.get_league_data(league_id, platform_id)
    except ValueError:
        flash("Unable to save league parameters!", "danger")
        return {"error": "Bad Request"}, 400

    if league_data is not None:
        if current_user.id not in league_data.get("admins", []) and not current_user.is_admin():
            flash("You are not allowed to do that", "danger")
            return {"error": "Forbidden"}, 403

        for admin_id in content.get("admins", []):
            league_admins.append(admin_id)
        league_db.set_league_form_data(league_id, platform_id, admins=league_admins, discord_invite=content.get("discord"),
                                       website_url=content.get("website"))
        flash("League parameters saved!", "success")
        return {"success": True}, 200

    flash("Unknown league", "danger")
    return {"error": "Bad Request"}, 400


@league.route("/admin_league", methods=["GET"])
@login_required
def admin_league():
    league_arg = request.args.get("league", request.args.get("league_id"))
    platform_arg = request.args.get("platform", request.args.get("platform_id"))

    if platform_arg is None or league_arg is None:
        return render_template("leagues/league.html")
    league_db = Leagues()
    rsrc_db = ResourcesRequest()
    user_db = Users()

    try:
        platform_id = int(platform_arg)
    except ValueError:
        platform_id = rsrc_db.get_platform_id(platform_arg)

    try:
        league_id = int(league_arg)
        league_data = league_db.get_league_data(league_id, platform_id)
    except ValueError:
        league_data = league_db.get_league_data_with_name(league_arg, platform_id)

    if league_data is not None:
        if current_user.id not in league_data.get("admins", []) and not current_user.is_admin():
            return render_template("error.html", error_code="Forbidden", error_description="You are not allowed to admin this league.")

        league_data["platform"] = rsrc_db.get_platform_label(platform_id)

        if league_data.get("logo") is None:
            league_data["logo"] = "Neutre_04"

        if "https://discordapp.com" in request.headers.get("user-agent"):
            return render_template("discord_views/league.html", league_data=league_data)

        try:
            admin_users = user_db.get_users([ObjectId(i) for i in league_data.get("admins", [])])
            league_data["admins"] = admin_users
        except TypeError:
            admin_users = user_db.get_discord_users(league_data.get("admins", []))
            league_data["admins"] = admin_users

        return render_template("leagues/admin_league.html", title=league_data.get("name", None), league_data=league_data)
    return render_template("error.html", error_code="League not found",
                           error_description="The requested league was not found on the server. If you entered the URL manually please check your "
                                             "spelling and try again.")


@league.route("/ranked_playoffs_winners")
def get_ranked_playoffs_winners():
    user_agent = request.headers.get("user-agent")
    platform_id = int(request.args.get("platform_id"))
    league_db = Leagues()
    competition_db = Competitions()
    rsrc_db = ResourcesRequest()
    if platform_id in [1, 2]:
        league_id = 1
    else:
        league_id = 3
    league_data = league_db.get_league_data(league_id, platform_id)
    if league_data is not None:
        if "https://discordapp.com" in user_agent:
            return render_template("discord_views/league.html", league_data=league_data)

        league_data["platform"] = rsrc_db.get_platform_label(platform_id)
        playoffs_ids = league_data.get("ccl_playoffs")
        competition_list = competition_db.get_competitions_data(league_id, playoffs_ids, platform_id, ['winner','runner_up'])
        competition_list.insert(7, None)
        competition_list.pop()
        competition_list.reverse()

        win_race = {}
        win_coach = {}
        for competition in competition_list:
            if competition is not None:
                winner_race = competition.get("winner", {}).get("race")
                winner_coach = competition.get("winner", {}).get("coach_id")
                if winner_race is not None:
                    if win_race.get(winner_race) is None:
                        win_race[winner_race] = {"name": winner_race, "number": 0}
                    win_race[winner_race]["number"] += 1

                if winner_coach is not None:
                    if win_coach.get(winner_coach) is None:
                        win_coach[winner_coach] = {"coach_id": winner_coach, "coach_name": competition.get("winner", {}).get("coach_name"),
                                                   "number": 0, "races": []}
                    win_coach[winner_coach]["number"] += 1
                    win_coach[winner_coach]["races"].append(winner_race)

        win_race = list(win_race.values())
        win_race = sorted(win_race, key=lambda i: i["number"], reverse=True)
        win_coach = list(win_coach.values())
        win_coach = sorted(win_coach, key=lambda i: i["number"], reverse=True)

        return render_template("leagues/past_winners.html", title="Champion Cup Winner", league_data=league_data, competition_list=competition_list,
                               win_race=win_race, win_coach=win_coach)
    return render_template("error.html", error_code="League not found",
                           error_description="The requested league was not found on the server. If you entered the URL manually please check your "
                                             "spelling and try again.")
