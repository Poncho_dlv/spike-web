from crawlerdetect import CrawlerDetect
from flask import Blueprint, request, render_template

team_creator = Blueprint('team_creator', __name__)


@team_creator.before_request
def before_request():
    user_agent = request.headers.get("user-agent")
    crawler_detect = CrawlerDetect(user_agent=user_agent)
    if crawler_detect.isCrawler():
        return crawler_detect.getMatches()


@team_creator.route("/team_creator")
def team_creator_app():
    return render_template('team_creation.html')
