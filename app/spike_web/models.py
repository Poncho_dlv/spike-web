from bson.errors import InvalidId
from flask_login import UserMixin, AnonymousUserMixin

from spike_user_db.Users import Users, AccountLvl, AccountType
from spike_web import login_manager


@login_manager.user_loader
def load_user(user_id):
    try:
        user = User()
        user.load_user(user_id)
        if user.id is not None:
            return user
    except InvalidId:
        pass
    return AnonymousUserMixin()


class User(UserMixin):
    def __init__(self):
        self.id = None
        self.username = None
        self.lang = []
        self.country = None
        self.avatar_url = None
        self.access_level = AccountLvl.STANDARD
        self.youtube = None
        self.twitch = None
        self.steam = None
        self.discord_id = None
        self.blood_bowl_coaches = []
        self.naf_name = None
        self.naf_number = None
        self.token = None

    def is_premium(self):
        return AccountLvl.PREMIUM in self.access_level

    def is_admin(self):
        return AccountLvl.ADMIN in self.access_level

    def is_tournament_director(self):
        return AccountLvl.TOURNAMENT_DIRECTOR in self.access_level

    def is_steam_linked(self):
        try:
            return int(self.steam["id"]) > 0
        except:
            return False

    def load_user(self, user_id: str):
        user_db = Users()
        user = user_db.get_user(user_id)
        if user is not None and user.get("type") == AccountType.DISCORD:
            self.__load_discord_user_from_raw(user)

    def load_discord_user(self, discord_id: int):
        user_db = Users()
        user = user_db.get_discord_user(discord_id)
        self.__load_discord_user_from_raw(user)

    def __load_discord_user_from_raw(self, user_data):
        if user_data is not None:
            self.id = str(user_data.get("_id"))
            self.username = user_data.get("user_name")
            self.lang = user_data.get("lang", [])
            self.country = user_data.get("country")
            self.type = AccountType.DISCORD
            self.youtube = user_data.get("youtube")
            self.steam = user_data.get("steam")
            self.twitch = user_data.get("twitch")
            self.naf_name = user_data.get("naf_name")
            self.naf_number = user_data.get("naf_number")
            self.discord_id = user_data.get("discord_id")
            self.avatar_url = "https://cdn.discordapp.com/avatars/{}/{}.png".format(user_data.get("discord_id"), user_data.get("avatar"))
            self.access_level = AccountLvl(user_data.get("level", AccountLvl.STANDARD))
            self.blood_bowl_coaches = user_data.get("blood_bowl_coaches", [])
            self.discord_token = user_data.get("token")
