from flask import session
from requests_oauthlib import OAuth2Session

from spike_settings.SpikeSettings import SpikeSettings


def token_updater(token):
    session['oauth2_token'] = token


def make_session(token=None, state=None, scope=None):
    return OAuth2Session(
        client_id=SpikeSettings().get_discord_oauth2_client_id(),
        token=token,
        state=state,
        scope=scope,
        redirect_uri=SpikeSettings().get_discord_oauth2_redirect_uri(),
        auto_refresh_kwargs={
            'client_id': SpikeSettings().get_discord_oauth2_client_id(),
            'client_secret': SpikeSettings().get_discord_oauth2_client_secret(),
        },
        auto_refresh_url=SpikeSettings().get_discord_token_url(),
        token_updater=token_updater)
