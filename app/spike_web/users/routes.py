from datetime import timedelta

from flask import render_template, url_for, redirect, request, Blueprint, session, jsonify, flash
from flask_login import login_user, current_user, logout_user, login_required

from spike_database.Coaches import Coaches
from spike_settings.SpikeSettings import SpikeSettings
from spike_user_db.Users import Users
from spike_utilities.Utilities import Utilities as CommonUtils
from spike_web.models import User
from spike_web.modules import Utilities
from spike_web.users.utils import make_session

users = Blueprint('users', __name__)


@users.route("/save_profile", methods=['POST'])
@login_required
def save_profile():
    user_db = Users()
    content = request.json
    success = True
    if Utilities.is_valid_youtube_channel(content.get("youtube")):
        youtube = Utilities.extract_id_from_url(content.get("youtube"))
    elif len(content.get("youtube")) > 0:
        return {"error": "Invalid Youtube channel"}, 460  # Custom error for youtube
    else:
        youtube = ""

    naf_name = content.get("naf_name")
    naf_number = content.get("naf_number")

    user_db.update_user(current_user.id, country=content.get("country"), lang=content.get("lang"), youtube=youtube, naf_name=naf_name,
                        naf_number=naf_number)
    flash('Profile updated', 'success')
    return {"success": success}, 200


@users.route("/profile", methods=['GET'])
@login_required
def profile():
    coach_db = Coaches()
    linked_coaches = []
    for coach in current_user.blood_bowl_coaches:
        ret_coach = coach_db.get_coach_name(coach.get("coach_id"), coach.get("platform_id"))
        if ret_coach is not None:
            coach_data = {
                "id": coach.get("coach_id"),
                "name": ret_coach,
                "platform_id": coach.get("platform_id")
            }
            linked_coaches.append(coach_data)

    return render_template('profile.html', title="{} Profile".format(current_user.username), CommonUtils=CommonUtils, linked_coaches=linked_coaches)


@users.route("/login")
def login():
    user_agent = request.headers.get("user-agent")
    if "https://discordapp.com" in user_agent:
        return render_template('discord_views/default.html')

    scope = ["identify", "guilds", "guilds.members.read"]
    discord = make_session(scope=scope)
    authorization_url, state = discord.authorization_url(SpikeSettings().get_discord_authorization_base_url())
    session["discord_oauth2_state"] = state
    session['next'] = request.args.get('next')
    return redirect(authorization_url)


@users.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("main.home"))


@users.route("/unlink_coach", methods=['GET'])
@login_required
def unlink_coach():
    coach_id = request.args.get("coach_id")
    platform_id = request.args.get("platform_id")
    user_db = Users()

    try:
        coach_id = int(coach_id)
        platform_id = int(platform_id)
        ret = user_db.remove_bb_coach_link(current_user.id, coach_id, platform_id)
        if ret is not None and ret.modified_count == 1:
            flash('Coach successfully unlinked', 'success')
        else:
            flash('Unable to unlink this coach', 'danger')
    except:
        flash('Unable to unlink this coach', 'danger')

    return redirect(url_for("users.profile"))


@users.route("/link_coach", methods=['GET'])
@login_required
def link_coach():
    coach_id = int(request.args.get("coach_id"))
    platform_id = int(request.args.get("platform_id"))
    if current_user.discord_id is not None:
        coach_db = Coaches()
        user_db = Users()
        if coach_id is not None:
            if coach_db.coach_exist(coach_id, platform_id):
                linked_user = user_db.get_user_linked(coach_id, platform_id)
                if linked_user is None:
                    ret = user_db.add_bb_coach_link(current_user.id, coach_id, platform_id)
                    if ret is not None and ret.modified_count == 1:
                        flash('Coach successfully linked', 'success')
                    else:
                        flash('Unable to link this coach', 'danger')
                    return redirect(url_for("users.profile"))
                error_code = "Link error"
                error_description = "Unable to link this coach with your discord account, this coach is already linked to a discord account. Id: {}".format(
                    linked_user)
            else:
                error_code = "Link error"
                error_description = "Unable to link this coach with your discord account, coach not found!"
        else:
            error_code = "Link error"
            error_description = "Unable to link this coach with your discord account, coach not found!"
    else:
        error_code = "Link error"
        error_description = "Unable to link this coach with your discord account, discord account not linked"
    return render_template("error.html", error_code=error_code, error_description=error_description)


@users.route("/callback")
def discord_callback():
    if request.values.get('error'):
        error_type = request.values.get('error')
        if error_type == "access_denied":
            return redirect(url_for("main.home"))
        error_description = request.values.get('error_description')
        return render_template("error.html", error_code=error_type, error_description=error_description)

    discord = make_session(state=session.get("discord_oauth2_state"))
    token = discord.fetch_token(SpikeSettings().get_discord_token_url(), client_secret=SpikeSettings().get_discord_oauth2_client_secret(),
                                authorization_response=request.url)
    session["discord_oauth2_token"] = token

    discord = make_session(token=token)
    base_url = SpikeSettings().get_discord_api_base_url()
    discord_user = discord.get(f"{base_url}/users/@me").json()
    discord_guild = discord.get(f"{base_url}/users/@me/guilds").json()
    session["guilds"] = discord_guild
    discord_id = int(discord_user.get("id"))

    user = User()
    user_db = Users()
    user.load_discord_user(discord_id)

    if user.id is None:
        user_db.add_discord_user(discord_user.get("username"), discord_id, discord_user.get("locale"), discord_user.get("discriminator"),
                                 discord_user.get("avatar"), token)
        user.load_discord_user(discord_id)
    else:
        user_db.update_user(user.id, user_name=discord_user.get("username"), avatar=discord_user.get("avatar"), token=token)

    if user.id is not None:
        login_user(user, remember=True, duration=timedelta(days=30))
    else:
        flash('Unable to login', 'danger')
    return redirect(session.get("next") or url_for("main.home"))


@users.route("/search/users", methods=['GET'])
def search_users():
    user_db = Users()
    query = request.args.get("query")
    suggestions = {}
    if len(query) > 2:
        ret_users = user_db.get_users_like(query, 9)
        suggestions = []
        for user in ret_users:
            suggestions.append(
                {
                    "value": user.get("user_name"),
                    "data": {
                        "name": user.get("user_name"),
                        "id": str(user.get("_id"))
                    }
                }
            )
        suggestions = jsonify({"suggestions": suggestions})
    return suggestions
