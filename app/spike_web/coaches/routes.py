import re

from crawlerdetect import CrawlerDetect
from flask import render_template, Blueprint, request, jsonify

from spike_database.Coaches import Coaches
from spike_database.CoachesStatistics import CoachesStatistics, StatisticType
from spike_database.MatchHistory import MatchHistory
from spike_database.ResourcesRequest import ResourcesRequest
from spike_requester.SpikeAPI.CclAPI import CclAPI
from spike_requester.SpikeAPI.StatisticsAPI import StatisticsAPI
from spike_requester.Utilities import Utilities as RequestUtils
from spike_user_db.Users import Users, AccountType
from spike_utilities.Statistics import STATISTICS_VERSION, LOW_TV_THRESHOLD, MID_TV_THRESHOLD, HIGH_TV_THRESHOLD
from spike_web.modules import Utilities
from web_utils.resources import platform_display

coaches = Blueprint('coaches', __name__)

tv_range_title = {
    "all_tv": "Overall",
    "low_tv": "Team value: 0-{}".format(LOW_TV_THRESHOLD),
    "mid_tv": "Team value: {}-{}".format(LOW_TV_THRESHOLD, MID_TV_THRESHOLD),
    "high_tv": "Team value: {}-{}".format(MID_TV_THRESHOLD, HIGH_TV_THRESHOLD),
    "very_high_tv": "Team value: {}+".format(HIGH_TV_THRESHOLD)
}


def sort_races(stats):
    for tv_range in tv_range_title.keys():
        stats[tv_range]["overall"]["races_for"] = list(stats[tv_range]["overall"]["races_for"].values())
        stats[tv_range]["overall"]["races_for"] = sorted(stats[tv_range]["overall"]["races_for"], key=lambda i: i["games_played"], reverse=True)

        stats[tv_range]["overall"]["races_against"] = list(stats[tv_range]["overall"]["races_against"].values())
        stats[tv_range]["overall"]["races_against"] = sorted(stats[tv_range]["overall"]["races_against"], key=lambda i: i["games_played"],
                                                             reverse=True)


@coaches.before_request
def before_request():
    user_agent = request.headers.get("user-agent")
    crawler_detect = CrawlerDetect(user_agent=user_agent)
    if "https://discordapp.com" not in user_agent and crawler_detect.isCrawler():
        return crawler_detect.getMatches()


@coaches.route("/platforms/<platform_id>/coaches/<coach_id>/overall_statistics", methods=['GET'])
def get_overall_statistic(platform_id, coach_id):
    try:
        coach_id = int(coach_id)
        platform_id = int(platform_id)
        coach_stat = CoachesStatistics()
        stats = coach_stat.get_all_range_coach_statistic(coach_id, platform_id, version=STATISTICS_VERSION)
        sort_races(stats)
        return render_template("coaches/statistic_view.html", statistics=stats, tv_range_title=tv_range_title, table_label="overall")
    except:
        return ""


@coaches.route("/platforms/<platform_id>/coaches/<coach_id>/ranked_statistics", methods=['GET'])
def get_ranked_statistic(platform_id, coach_id):
    try:
        coach_id = int(coach_id)
        platform_id = int(platform_id)
        coach_stat = CoachesStatistics()
        stats = coach_stat.get_all_range_coach_statistic(coach_id, platform_id, stat_type=StatisticType.RANKED_LADDER, version=STATISTICS_VERSION)
        sort_races(stats)
        return render_template("coaches/statistic_view.html", statistics=stats, tv_range_title=tv_range_title, table_label="ranked")
    except:
        return ""


@coaches.route("/platforms/<platform_id>/coaches/<coach_id>/<entry_id>/<entry_name>/statistics", methods=['GET'])
def get_common_statistic(platform_id, coach_id, entry_id, entry_name):
    try:
        coach_id = int(coach_id)
        platform_id = int(platform_id)
        entry_id = int(entry_id)
        coach_stat_db = CoachesStatistics()
        stats = coach_stat_db.get_all_range_coach_statistic(coach_id, platform_id, StatisticType(entry_id), entry_name, STATISTICS_VERSION)
        sort_races(stats)
        return render_template("coaches/statistic_view.html", statistics=stats, tv_range_title=tv_range_title, table_label=str(entry_id))
    except:
        return ""


@coaches.route("/platforms/<platform_id>/coaches/<coach_id>/last_matches", methods=['GET'])
def get_last_matches(platform_id, coach_id):
    try:
        coach_id = int(coach_id)
        platform_id = int(platform_id)
    except:
        return ""

    match_history_db = MatchHistory()
    last_match = match_history_db.get_last_match(coach_id, platform_id, 50)
    coach_db = Coaches()
    if len(last_match) == 0:
        return ""

    coaches_id = []
    for match in last_match:
        coaches_id.append(match["team_home"]["coach_id"])
        coaches_id.append(match["team_away"]["coach_id"])
        match["date"] = Utilities.date_converter(match["date"])
    coaches_id = list(set(coaches_id))
    coaches_name = coach_db.get_coaches_name(coaches_id, int(platform_id))
    coaches_name[0] = "AI"
    return render_template("coaches/coach_last_match.html", last_match=last_match, coaches_name=coaches_name)


@coaches.route("/coach", methods=['GET'])
def coach():
    user_agent = request.headers.get("user-agent")
    platform_id = request.args.get('platform_id')
    coach_id = request.args.get('coach_id')
    match_uuid = request.args.get("match_uuid")
    display_coach = request.args.get("display_coach")

    if (match_uuid is not None and display_coach in ["home", "away"]) or (platform_id is not None and coach_id is not None):
        if match_uuid is not None and display_coach is not None:
            match = RequestUtils.get_match(match_uuid)
            platform_id = match.get_platform_id()
            if display_coach == "home":
                coach_id = match.get_coach_home().get_id()
            else:
                coach_id = match.get_coach_away().get_id()
        try:
            coach_id = int(coach_id)
            platform_id = int(platform_id)
        except ValueError:
            return render_template('error.html', error_code="Coach not found",
                                   error_description="The requested coach was not found on the server. If you entered the URL manually please check "
                                                     "your spelling and try again.")

        coach_db = Coaches()
        coach_stats_db = CoachesStatistics()
        coach_data = coach_db.get_coach_data(coach_id=coach_id, platform_id=platform_id)
        if coach_data is not None:
            coach_stat = StatisticsAPI.get_win_rate(coach_id, platform_id)

            user_db = Users()
            user_linked_id = user_db.get_user_linked(coach_id, platform_id)
            if user_linked_id is not None:
                user_linked = user_db.get_user(str(user_linked_id))
                if user_linked.get("type") == AccountType.DISCORD and user_linked.get("avatar"):
                    user_linked["avatar_url"] = "https://cdn.discordapp.com/avatars/{}/{}.png".format(user_linked.get("discord_id"),
                                                                                                      user_linked.get("avatar"))
            else:
                user_linked = None

            if "https://discordapp.com" in user_agent:
                rsrc_db = ResourcesRequest()
                coach_data["platform"] = rsrc_db.get_platform_label(platform_id)
                return render_template('discord_views/coach.html', coach_data=coach_data, coach_stat=coach_stat, user_linked=user_linked)

            if len(coach_data.get("other_names", [])) > 1:
                coach_data["other_names"].remove(coach_data.get("name", None))
            else:
                coach_data["other_names"] = None

            rsrc_db = ResourcesRequest()
            coach_data["platform"] = rsrc_db.get_platform_label(coach_data.get("platform_id"))

            ranked_seasons = CclAPI.get_seasons(platform_id)
            coach_cmp_list = coach_stats_db.get_competitions_played(coach_id, platform_id, STATISTICS_VERSION)
            coach_lg_list = coach_stats_db.get_leagues_played(coach_id, platform_id, STATISTICS_VERSION)

            def populate_ranked(competition_list: list):
                output_list = []
                for current_season in competition_list:
                    if current_season.get("name") in coach_cmp_list:
                        coach_cmp_list.remove(current_season.get("name"))
                        output_list.append({"name": current_season.get("name"), "active": True})
                    else:
                        output_list.append({"name": current_season.get("name"), "active": False})
                output_list.reverse()
                return output_list

            ranked_ladders = populate_ranked(ranked_seasons.get('data', {})["ladders"])
            ranked_playoffs = populate_ranked(ranked_seasons.get('data', {})["playoffs"])

            return render_template('coaches/coach.html', title=coach_data.get("name", None), coach_data=coach_data, user_linked=user_linked,
                                   coach_stat=coach_stat, ranked_ladders=ranked_ladders, ranked_playoffs=ranked_playoffs,
                                   tv_range_title=tv_range_title, coach_cmp_list=coach_cmp_list, coach_lg_list=coach_lg_list)
        return render_template('error.html', error_code="Coach not found",
                               error_description="The requested coach was not found on the server. If you entered the URL manually please check your "
                                                 "spelling and try again.")
    return render_template('coaches/search.html')


@coaches.route("/search/coaches", methods=['GET'])
def process():
    query = request.args.get('query')
    suggestions = {}
    # special characters
    special_char = re.compile(r'[@_!#$%^&*()<>?/\|}{~:]')
    if special_char.search(query) is None and len(query) > 2:
        coach_db = Coaches()
        suggestions = coach_db.get_filtered_coaches_like(query, limit=9)
        for suggestion in suggestions:
            suggestion["value"] = "{} [{}]".format(suggestion["value"], platform_display.get(str(suggestion["data"]["platform_id"])))
        suggestions = jsonify({"suggestions": suggestions})
    return suggestions
